#! /usr/bin/perl

BEGIN {
       use CGI::Carp qw/carpout/;
       open LOG, ">>", "/tmp/carp.log" or die("Cannot open file: $!\n");
       carpout(LOG);
}

use RINGS::Glycan::KCF;
use RINGS::Web::HTML;
use RINGS::Web::user_admin;
use RINGS::Web::URL;
use CGI;
use CGI::Carp qw(fatalsToBrowser);

$CGI::POST_MAX = 1024 * 1024;           # 1024 * 1KBytes = 1MBytes.

$DIR = "/userdata/profiles/";

require "../share/cgi-lib.pl";



my $dbh = &Connect_To_Rings;
print "Content-type: text/html\n\n";

print "<html>\n";
print "<head>\n";
my $title = "Kernel Tool input data";
my $head = Make_Head_From_Title($title);
print "$head\n";
print "</head>\n";
print "<body>\n";
my $h1 = Make_H1_From_Header($title);
print "$h1";
print "<br>\n";


print "<form action=\"/cgi-bin/tools/user_admin/tool_open.pl\" method=\"POST\">";
#######################
# Get UserID & DataID #
#######################

$user_id = Get_Cookie_Info();

if($user_id != 0){
	$DIR = "/var/www/userdata/$user_id/KernelTool/";
}
my $output_id = $ENV{'QUERY_STRING'};
my @array ="";
if($output_id =~ /\//){
        @array = split(/\//,$output_id);
        shift @array;
        $output_id = shift @array;
        my $inputdata = shift @array;
}


my $type = "Two KCFs";
my @result = Get_Tools_Name($dbh, $type);

print "<select name='tool_name'>\n";
foreach (@result){
	print "<option value='$_'>$_</option>\n";
}
print "</select>";
print "<input type = 'submit' value = 'GO'>\n";
print "";

my $control_path = "/var/www/userdata/$user_id/KernelTool/$output_id/control.txt";
print "<div align='right'><a href=\"$control_path\" >Download</a></div><p>";
print "<h3>Control Data</h3>\n";
&output_inputKCF('image',$control_path);
print "<input type = \"hidden\" name = \"control_path\" value = \"$control_path\">";

my $target_path = "/var/www/userdata/$user_id/KernelTool/$output_id/target.txt";
print "<div align='right'><a href=\"$target_path\" >Download</a></div><p>";
print "<h3>Target Data</h3>\n";
&output_inputKCF('image',$target_path);
print "<input type = \"hidden\" name = \"target_path\" value = \"$target_path\">";

  
print <<EOF;
</center>
</form>
</body>
</html>
EOF

my @entry = "";
my $url = "";


sub output_inputKCF {

   my $tag = shift @_; #input tag : Text or Image
   $path = shift @_;
   if(open(DIR,$path)){

        my $glycan = "";
        if($tag eq "image"){
                print <<EOF;
<table width = 800 border="1" cellspacing="1" cellpadding="3">
<tr class = "bgc_glay">
<th class = "bgc_glay">Name </th>
<th class = "bgc_glay">Input structure</th>
</tr>
EOF
        }
        while(<DIR>){
                if ($_ =~ /ENTRY/){
                        @entry = split(/\s+/, $_);
                        $url = Get_URL_From_Gname($entry[1]);
                }

                if($_ =~ /^\/\/\//){
                        $glycan .= $_;
                        if($tag eq "image"){
                                print "<tr><td><a href=\"$url\">$entry[1]</td>\n";
                                #my $imgtag = Get_Img_Tag_From_KCF($dbh,$glycan,0);
                                my $imgtag = get_img_tag_from_kcf($dbh,$glycan,0);
                                print "<td>$imgtag</td></tr>\n";
                        }else{
                                print $glycan;
                        }
                        $glycan = "";
		 }else{
                        $glycan .= $_;
                }
        }

        if($tag eq "image"){
                print "</table>";
        }
         #  print "</pre>";
        close(DIR);
   }else{
      if($tag eq "image"){
         print "<div align = \"center\"><font size = 3><B>Error: No input glycan data at $path.</B></font></div>";
      }else{
          print "Error: No input glycan data at $path.";
      }

   }

}

    



