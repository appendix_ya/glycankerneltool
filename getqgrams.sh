#!/bin/sh

for nvar in 1 2 3 4 5 6 7 8 9
do
   if test $nvar -ge $2 -a $nvar -le $3; then
      outfile="${1%.txt}-$nvar.txt"
      `ruby getqgram_file.rb $1 $nvar $nvar > $outfile`
   fi 
done

