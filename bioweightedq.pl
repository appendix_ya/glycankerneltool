#! /usr/bin/perl

use RINGS::Web::HTML;
$id = Get_Cookie_Info();
use RINGS::Tool;
use RINGS::Glycan::KCF;
use RINGS::Glycan::Glycan;
use RINGS::Glycan::Mapping;
use File::Find;
use File::Copy;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use warnings;
require 5.001;
require "../share/cgi-lib.pl";
$CGI::POST_MAX = 1024 * 1024 * 16;       # 1024 * 1KBytes = 1MBytes.




if ($ENV{'REQUEST_METHOD'} eq 'POST') {
  read(STDIN, $alldata, $ENV{'CONTENT_LENGTH'});
} else {
  $alldata = $ENV{'QUERY_STRING'};
}
foreach $data (split(/&/, $alldata)) {
  ($key, $value) = split(/=/, $data);

  $value =~ s/\+/ /g;
  $value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack('C', hex($1))/eg;
  $value =~ s/\t//g;

  $in{"$key"} = $value;
}





# ===================
# Global Parameters
# ===================
 $ALPHA = "";
 $METHOD_NUMBER = 2;        # weighted (LK) :1, bio-weighted (LKR) :2, both:3
 $STATUS = 3;	#calculation status: 1)during calculation, 2)Error, 3)Finish calculation
 $CLASSNAME = "";
 $SUBTREE_MIN = "";
 $SUBTREE_MAX = "";
 $DBH = Connect_To_Rings;



# ===================
# Output Header
# ===================
print "Content-type: text/html\n\n";
print "<html>\n";
print "<head>\n";
my $title = "Kernel Training: Result";
my $head = Make_Head_From_Title($title);
print "$head\n";
my $h1 = Make_H1_From_Header($title);
print "$h1";
print "<br>\n";

print <<EOF;
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="Content-Style-Type" content="text/css">
<title>Kernel Result</title>
<style type="text/css">
loading {
	position: absolute;
	top: 35%;	
	left: 45%;
	font-style: italic;
	font-family: Verdana;
}
</style>
</head>
<body>
EOF






# ==================
# Get form data
# ==================
  $id = $in{'calc_id'};
  




################################
#            Main              #
################################
##### main excute #####

 #Update_K($DBH, $id, $STATUS);
 ($METHOD_NUMBER, $STATUS, $ALPHA, $CLASSNAME, $SUBTREE_MIN, $SUBTREE_MAX) = Select_from_K($DBH, $id);

my @result_list = ();
my @result_hash;

my $result_file = "/tmp/kernel/$id/LKR_result.txt";

open(IN, $result_file);
@result = <IN>;
foreach my $result_line (@result){
   $result_line =~ s/\s+/ /g;
   if($result_line =~ /^\s(\d.+)$/){
      $result_line = $1;
   }
   @result_list = split(/\s/, $result_line);
   $result_hash{$result_list[0]} = $result_list[1];
}
close(IN);
@keys = sort {$result_hash{$b} <=> $result_hash{$a} || length($b) <=> length($a) || $a cmp $b } keys %result_hash;
&output(@keys);

#exit;















##################
# Get post data  #
##################
sub get_post_data {
 $formdata = new CGI;
 $formdata->param;  #error processing
 die($formdata->cgi_error) if ($formdata->cgi_error);

 my $idLKR = $formdata->param('calc_ID');  #Get Class Name

 print"id = $idLKR";

 # ===================================
 # Error and Default value Processing
 # ===================================
 $error = "";

 if($idLKR eq ""){
    $error .= "Please input Kernel calculation ID.<br>";
    &printHeader("ERROR: $error");
    exit;
 }

 my @postdatas = ($idLKR);
 @postdatas;
}






#################
#  Output	#
#################
sub output {
#   my @sorted_keys = shift @_;


print <<EOF;
<div id="doc">
   <table width = 1000 border=0 cellpadding=0 cellspacing=0>
   <tr valign = "top">
      <td width = 150>
         <h4 style = "text-align : center;" align = "center">
            <a href="/index.html" onMouseover="change(this,'#9966FF','#330066')" onMouseout="change(this,'#8484ee','#000033')">Home</a>
         </h4>
         <h4 style="text-align : center;" align="center">
            <a href="/help/help.html" onMouseover="change(this,'#9966FF','#330066')" onMouseout="change(this,'#8484ee','#000033')">Help</a>
         </h4>
         <h4 style="text-align : center;" align="center">
EOF
	    my $bugTarget = "BugDisplay.pl";
	    if($id != 0){
	       $bugTarget = "BugReportForm.pl?tool=Glycan Kernel Tool";
	    }
print <<EOF;
            <a href = "/cgi-bin/tools/Bug/$bugTarget" onMouseover="change(this,'#9966FF','#330066')" onMouseout="change(this,'#8484ee','#000033')">Feedback</a>
         </h4>
      </td>

   <td width = 800>
   <Font size = 3>
      Calculation ID: $id<BR>
      Data set name: $CLASSNAME<BR>
      A range of subtree: $SUBTREE_MIN <= size <= $SUBTREE_MAX<BR><BR>

   	  *Layer indicates the depth of a monosaccharide from the reducing end. 
	  (<a href="http://www.ncbi.nlm.nih.gov/pubmed/22347783">Jiang et al., 2011</a>)
   </Font>

   <Font size = 4 align = "center"><B>
   <TABLE border = 3 cellpadding = 3 cellspacing = 0>
   <TBODY>
      <TR>
         <TD class = "bgc_glay"><B><FONT size = "3">No.</FONT></B></TD>
         <TD class = "bgc_glay"><B><FONT size = "3">Score</FONT></B></TD>
         <TD class = "bgc_glay"><B><FONT size = "3">Layer</FONT></B></TD>
         <TD class = "bgc_glay"><B><FONT size = "3"><center>Feature</center></FONT></B></TD>
      </TR>
</div>
</body>
EOF


   # =================================
   my $count = 1;
   my $ksubtree = "";
   my $kcfimage = "";
   my $layer = "";
   foreach $subtree (@keys) {
      my $score = $result_hash{$subtree};
      my $layer = -1;
      my $kcf = "";
      my $imgtag = "";
      if($subtree =~ /^(\d+)-(.+)$/){
         $layer = $1;
         $ksubtree = $2;

         $kcf = &get_kcf($ksubtree);
         $imgtag = get_img_tag_from_kcf($DBH, $kcf);
      }


print <<EOF;
     <TR>
       <TD>$count</TD>
       <TD>$score</TD>
       <TD>$layer</TD>
       <TD>$imgtag</TD>
    </TR>
EOF
   $count++;

   }

print <<EOF;
   </TBODY>
   </TABLE>
   </Font>
   </td>
   </tr>
   </table>
   </body>
   </html>
EOF
}




############################################
#  print Header (Header for error output)  #
############################################
sub printHeader() {
print <<EOF;

   <table width = 1000 border=0 cellpadding=0 cellspacing=0>
   <tr valign="top">
      <td width = 150>
         <h4 style = "text-align : center;" align = "center">
            <a href="/index.html" onMouseover="change(this,'#9966FF','#330066')" onMouseout="change(this,'#8484ee','#000033')">Home</a>
         </h4>
         <h4 style="text-align : center;" align="center">
            <a href="/help/help.html" onMouseover="change(this,'#9966FF','#330066')" onMouseout="change(this,'#8484ee','#000033')">Help</a>
         </h4>
         <h4 style="text-align : center;" align="center">
EOF
	    my $bugTarget = "BugDisplay.pl";
	    if($id != 0){
	       $bugTarget = "BugReportForm.pl?tool=Glycan Kernel Tool";
	    }
print <<EOF;
            <a href = "/cgi-bin/tools/Bug/$bugTarget" onMouseover="change(this,'#9966FF','#330066')" onMouseout="change(this,'#8484ee','#000033')">Feedback</a>
         </h4>
      </td>
EOF

 print shift @_; #output error messages.
 print "<B></td></tr></table></body></html>";
 exit;
}










#--------------------------------------------------
# changing kernel result format to LinearCode
#    Although, each monosacchride name is remained
#--------------------------------------------------
sub get_kcf{
   my $subst = shift @_;
   my $lcode = "";
   my $size_subst = length($subst);
   my $i = 1;

   while($size_subst > 0){
      $i++;
      if($subst =~ /^([^<\[>\]]+)([<\[].+)$/){
         $lcode = "$1".$lcode;
         $subst = $2;
         $size_subst = length($subst);

      }elsif($subst =~ /^(\[[\/-\w]+\])([a-z<].*)$/){
         $lcode = "$1".$lcode;
         $subst = $2;
         $size_subst = length($subst);

      }elsif($subst =~ /^(<)(\[.+)$/){
         $lcode = ")".$lcode;
         $subst = $2;
         $size_subst = length($subst);

      }elsif($subst =~ /^([^<\[>\]]+)(>+<.+)$/){
         $lcode = "$1".$lcode;
         $subst = $2;
         $size_subst = length($subst);

      }elsif($subst =~ /^(>)(.+)$/){
         $lcode = "(".$lcode;
         $subst = $2;
         $size_subst = length($subst);

      }elsif($subst =~ /^([^<\[>\]]+)(>+)$/){
         $lcode = "$1".$lcode;
         $subst = $2;
         $size_subst = length($subst);

      }elsif($subst =~ /^(>)$/){
         $lcode = "(".$lcode;
         $subst = "";
         $size_subst = 0;

      }elsif($subst =~ /^([\w\-]+)$/){
         $lcode = "$1".$lcode;
         $subst = "";
         $size_subst = 0;
      }
   }
   my $KCF = &makeKCF($lcode);
   return $KCF;
}


#-----------------------------------------------------
#  get KCF format from Kernel result:
#  using /usr/lib/perl5/RINGS/Glycan/KCF.pm (makeKCF)
#      -> copied and modified for kernel result format
#-----------------------------------------------------
sub makeKCF{
   my $lcode = shift @_;
   my ($oya,$ko,$edge_num,@bunki,@array,%node,%pare,%edges,%tanto);

   my $num = 0;
   my $i = 1;

   while($lcode =~ /([^\[\]()]+)|(\[[^\[\]]+\])|(\)\()|([\)\(])/g){
      push(@array,$&);
   }

   foreach(reverse @array){
      if($_ =~ /^[^()\[\]]+$/){
         $tanto{$i} = $_;
         $num ++;

         if($i == 1){
            $oya = $i;
         }else{
            push(@{$pare{$oya}},$ko);
            $oya = $ko;
         }
         $i++;
      }elsif($_ =~ /\[(.+)\]/){
         $edge_num++;
         $ko = $i;
         $edges{$oya}{$ko} = $1;
      }else{
         if($_ =~ /\)\(/){
            $oya = $bunki[-1];
         }elsif($_ =~ /\)/){
            push(@bunki,$i-1);
         }elsif($_ =~ /\(/){
            $oya = pop(@bunki);
         }
      }
   }

   %habaHash = ();
   my %xHash = DFS(1,%pare);
   my %yHash = setYs(1,%pare);

   my $KCF = "ENTRY" . "               " . "Glycan\nNODE";
   $KCF = $KCF . "        " . "$num\n";

   foreach(sort {$a <=> $b} keys %tanto){
      $KCF = $KCF."            ".$_."     ".$tanto{$_}."     ".$xHash{$_}."     ".$yHash{$_}."\n";
   }

   $KCF = $KCF ."EDGE        ".$edge_num."\n";

   my $j = 1;
   foreach $o(sort {$a <=> $b} keys %edges){
      foreach $k(sort {$a <=> $b} keys %{$edges{$o}}){
         my($type,$c) = split(/-/,$edges{$o}{$k});
         $KCF = $KCF ."            ".$j."     "."$k:${type}"."     "."$o:$c\n";
         $j++;
      }
   }

   $KCF = $KCF . "///";
   return $KCF;
}




