#! /usr/bin/perl

use RINGS::Web::HTML;
$id = Get_Cookie_Info();
use RINGS::Tool;
use RINGS::Glycan::KCF;
use RINGS::Glycan::Mapping;
use File::Find;
use File::Copy;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use warnings;
require "../share/cgi-lib.pl";
$CGI::POST_MAX = 1024 * 1024 * 16;       # 1024 * 1KBytes = 1MBytes.



# ===================
# Global Parameters
# ===================
 $ID = "";
 $ALPHA = "";
 $METHOD_NUMBER= "";        # weighted (LK) :1, bio-weighted (LKR) :2, both:3
 $STATUS = "";	#calculation status: 1)during calculation, 2)Error, 3)Finish calculation
 $CLASSNAME = "";	#data set name which user input
 $SUBTREE_MIN = "";	#minimum size of subtree which user input
 $SUBTREE_MAX = "";	#maximum size of subtree which user input
 $method_name = "";
 $output_check = 0;
 $DBH = Connect_To_Rings();



# ===================
# Output Header
# ===================
print "Content-type: text/html\n\n";
print "<html>\n";
print "<head>\n";
my $title = "Kernel Training: Result";
my $head = Make_Head_From_Title($title);
print "$head\n";
my $h1 = Make_H1_From_Header($title);
print "$h1";
print "<br>\n";

print <<EOF;
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="Content-Style-Type" content="text/css">
<title>Kernel Result</title>

<style type="text/css">
loading {
	position: absolute;
	top: 35%;	
	left: 45%;
	font-style: italic;
	font-family: Verdana;
}
</style>
</head>
<body>
EOF






# ==================
# Get form data
# ==================
 my @postdatas = get_post_data();
 $ID = $postdatas[0];

 $SAVE_DIR = "/tmp/kernel/$ID";





################################
#            Main              #
################################
##### main excute #####

 #-------------------------------------------
 # gat/modify calculation status from RINGS Database
 #    if the calculation is finished (result_LK/LKR.txt is made),
 #    the STATUS will be 3.
 #-------------------------------------------

 ($METHOD_NUMBER, $STATUS, $ALPHA, $CLASSNAME, $SUBTREE_MIN, $SUBTREE_MAX) = Select_from_K($DBH, $ID);

 if($METHOD_NUMBER eq ""){
    $error .= "Please input correct Kernel calculation ID.<br>";
    &printHeader("ERROR: $error");
    exit;
 }

 if($METHOD_NUMBER ==1){
    $method_name = "weighted q-gram kernel";
 }elsif($METHOD_NUMBER ==2){
    $method_name = "bioweighted q-gram kernel";
 }elsif($METHOD_NUMBER ==3){
    $method_name = "weighted and bioweighted q-gram kernels";
 }
 
 
 my $erMesse = "$SAVE_DIR/matlabERROR.txt";
 open(FILE, "< $erMesse");
    my @list = <FILE>;
    foreach my $line (@list){
       if($line =~ /Error/){
          $STATUS = 2;
          Update_K($DBH, $ID, $STATUS);

          $error .= "Error: Please notify developers with your calculation ID via the Bug Report System.<br>";
          &printHeader("ERROR: $error");
       }
    }
 close(FILE);
 
 if($STATUS == 1){
    if($METHOD_NUMBER == 1){
       if(!open(F1,"$SAVE_DIR/LK_result.txt")){
	  $output_check = 1;
       }else{
          $STATUS = 3;
	  $output_check = 3;

#  	  #update user management system too
#     	  my $output_data_size = -s "$SAVE_DIR/LK_result.txt";
#
#          Update_K($DBH, $ID, $STATUS, $output_data_size);
       }
    }elsif($METHOD_NUMBER == 2){
       if(!open(F1,"$SAVE_DIR/LKR_result.txt")){
	  $output_check = 1;
       }else{
          $STATUS = 3;
	  $output_check = 3;

       }
    }elsif($METHOD_NUMBER == 3){
       if(!open(F1,"$SAVE_DIR/LK_result.txt") && !open(F2,"$SAVE_DIR/LKR_result.txt")){
	  $output_check = 1;
       }else{ 
          $STATUS = 3;
	  $output_check = 3;
#	  close(F1);
#	  close(F2);
#
#  	  #update user management system too
#     	  my $output_data_size = -s "$SAVE_DIR/LK_result.txt";
#     	  $output_data_size += -s "$SAVE_DIR/LKR_result.txt";
#
#          Update_K($DBH, $ID, $STATUS, $output_data_size);
       }
    }
 }elsif($STATUS == 2){
    $output_check = 2;
 }elsif($STATUS == 3){
    $output_check = 3;
 }

&output;


exit;









##################
# Get post data  #
##################
sub get_post_data {
 $formdata = new CGI;
 $formdata->param;  #error processing
 die($formdata->cgi_error) if ($formdata->cgi_error);

 my $ID = $formdata->param('id');  #Get Class Name


 # ===================================
 # Error and Default value Processing
 # ===================================
 $error = "";

 if($ID eq ""){
    $error .= "Please input Kernel calculation ID.<br>";
    &printHeader("ERROR: $error");
    exit;
 }

 my @postdatas = ($ID);
 @postdatas;
}







#################
#  Output	#
#################
sub output{

print <<EOF;
<div id="doc">
   <table width = 1000 border=0 cellpadding=0 cellspacing=0>
   <tr valign = "top">
      <td width = 150>
         <h4 style = "text-align : center;" align = "center">
            <a href = "/index.html" onMouseover = "change(this, '#9966FF', '#330066')" on Mouseout = "change(this, '#8484ee', '#000033')">Home</a>
         </h4>
         <h4 style = "text-align : center;" align = "center">
            <a href = "/help/help.html" onMouseover = "change(this, '#9966FF', '#330066')" on Mouseout = "change(this, '#8484ee', '#000033')">Help</a>
         </h4>
         <h4 style = "text-align : center;" align = "center">
EOF
	 my $bugTarget = "BugDisplay.pl";
	 if($id != 0){
	    $bugTarget = "BugReportForm.pl?tool=Glycan Kernel Tool";
	 }

print <<EOF;
            <a href = "/cgi-bin/tools/Bug/$bugTarget"
            onMouseover = "change(this, '#9966FF', '#330066')" on Mouseout = "change(this, '#8484ee', '#000033')">Feedback</a>
         </h4>
      </td>
   <td align = "center" width = 800><Font size = 4><B>
   <TABLE border = 3 cellpadding = 3 cellspacing = 0>
   <TBODY>
      <TR>
         <TD class = "bgc_glay"><B><FONT size = "3"><center>ID</center></FONT></B></TD>
         <TD>$ID</TD>
      </TR>
      <TR>
         <TD class = "bgc_glay"><B><FONT size = "3"><center>Data set name</center></FONT></B></TD>
         <TD>$CLASSNAME</TD>
      </TR>
      <TR>
         <TD class = "bgc_glay"><B><FONT size = "3"><center>Range of subtree</center></FONT></B></TD>
         <TD>$SUBTREE_MIN <= q <= $SUBTREE_MAX</TD>
      </TR>
      <!--TR>
         <TD class = "bgc_glay"><B><FONT size = "3"><center>kernel method(s)</center></FONT></B></TD>
         <TD>$method_name</TD>
      </TR-->
      <TR>
         <TD class = "bgc_glay"><B><FONT size = "3"><center>status</center></FONT></B></TD>
EOF

 if($output_check==1){
print <<EOF;
         <TD>Processing</TD>
EOF

 }elsif($output_check==2){
print <<EOF;
         <TD>Error: Your input data might be too large or please contact us via Feedback system.</TD>
EOF

 }elsif($output_check==3){
print <<EOF;
   <TD>calculation has been finished successfully.<br>
EOF

  	  #update user management system 
     	  my $output_data_size = -s "$SAVE_DIR/LKR_result.txt";

          my $output_id = Update_K($DBH, $ID, $STATUS, $output_data_size);

	  my $user_id = Get_Cookie_Info();

	  copy($SAVE_DIR."/LKR_result.txt", "/var/www/userdata/".$user_id."/KernelTool/".$output_id."/LKR_result.txt");


    if($METHOD_NUMBER==1){
print <<EOF;
   <br>
   <a href="./weightedq.pl?calc_id=$ID">weighted q-gram result</a><br>
EOF
#   <div id = "div_1">
#   <p><input type="button" value=show weighted q-gram result" style = "WIDTH:150px"
#      onClick="document.getElementByID('div_2').style.display='block';
#    	       document.getElementByID('div_1').style.display='none'"></p>
#   </div>
#   <div id="div_2" style="display:non">
#   <p><input type="button" value=hide weighted q-gram result" style = "WIDTH:150px"
#      onClick="document.getElementByID('div_2').style.display='none';
#       	       document.getElementByID('div_1').style.display='block'"></p>
#   <p>
#EOF
#       &result_output("LK");
#print <<EOF;
#   </p>
#   <p><input type="button" value=hide weighted q-gram result" style = "WIDTH:150px"
#      onClick="document.getElementByID('div_2').style.display='none';
#       	       document.getElementByID('div_1').style.display='block';
#      	       document.location='#div_1'"></p>
#   </div>
#EOF


#--------------------------------------------------------
    }elsif($METHOD_NUMBER==2){
print <<EOF;
   <br>
   <a href="./bioweightedq.pl?calc_id=$ID">bioweighted q-gram result</a><br>
EOF
#   <div id = "div_1">
#   <p><input type="button" value=show bioweighted q-gram result" style = "WIDTH:150px"
#      onClick="document.getElementByID('div_2').style.display='block';
#    	       document.getElementByID('div_1').style.display='none'"></p>
#   </div>
#   <div id="div_2" style="display:non">
#   <p><input type="button" value=hide bioweighted q-gram result" style = "WIDTH:150px"
#      onClick="document.getElementByID('div_2').style.display='none';
#       	       document.getElementByID('div_1').style.display='block'"></p>
#   <p>
#EOF
#       &result_output("LKR");
#print <<EOF;
#   </p>
#   <p><input type="button" value=hide bioweighted q-gram result" style = "WIDTH:150px"
#      onClick="document.getElementByID('div_2').style.display='none';
#       	       document.getElementByID('div_1').style.display='block';
#      	       document.location='#div_1'"></p>
#   </div>
#EOF


#--------------------------------------------------------
    }elsif($METHOD_NUMBER==3){
print <<EOF;
   <br>
   <a href="./weightedq.pl?calc_id=$ID">weighted q-gram result</a><br>

   <a href="./bioweightedq.pl?calc_id=$ID">bioweighted q-gram result</a><br>
EOF
   
    }
 }

print <<EOF;
   </dl>
   </TR>
   </TBODY>
   </TABLE>
   </td>
   </tr>
   </table>
   </body>
   </html>
EOF
}





sub result_output{
   my $type_name = @_;
   my @result_list = ();
   my @result_hash;

print <<EOF;
   <td align = "center" width = 800><Font size = 4>
   <TABLE border = 3 cellpadding = 3 cellspacing = 0>
       <TD class = "bgc_glay"><B><FONT size = "3">No.</FONT></B></TD>
       <TD class = "bgc_glay"><B><FONT size = "3">Score</FONT></B></TD>
       <TD class = "bgc_glay"><B><FONT size = "3">Layer</FONT></B></TD>
       <TD class = "bgc_glay"><B><FONT size = "3"><center>Feature</center></FONT></B></TD>
EOF

   my $result_file = "/tmp/kernel/$ID/result_$type_name.txt";

   open(IN, $result_file);
      @result = <IN>;
      foreach my $result_line (@result){
         $result_line =~ s/\s+/ /g;
         if($result_line =~ /^\s(\d.+)$/){
            $result_line = $1;
         }
         @result_list = split(/\s/, $result_line);
         $result_hash{$result_list[0]} = $result_list[1];
      }
   close(IN);
   my @keys = sort {$result_hash{$b} <=> $result_hash{$a} || length($b) <=> length($a) || $a cmp $b } keys %result_hash;

   my $count = 1;
   my $ksubtree = "";
   my $kcfimage = "";
   my $layer = "";
   foreach $subtree (@keys) {
      $score = $result_hash{$subtree};
      $layer = -1;
      if($subtree =~ /^(\d+)-(.+)$/){
         $layer = $1;
         $ksubtree = $2;
      }

print <<EOF;
     <td>
       <TD>$count</TD>
       <TD>$score</TD>
       <TD>$layer</TD>
       <TD>$ksubtree</TD>
    </td>
EOF

   }
}








############################################
#  print Header (Header for error output)  #
############################################
sub printHeader() {
print <<EOF;
   <table width = 1000 border=0 cellpadding=0 cellspacing=0>
   <tr valign = "top">
      <td width = 150>
         <h4 style = "text-align : center;" align = "center">
            <a href = "index.html" onMouseover = "change(this, '#9966FF', '#330066')" 
	    on Mouseout = "change(this, '#8484ee', '#000033')">Home</a>
         </h4>
         <h4 style = "text-align : center;" align = "center">
            <a href = "/help/help.html" onMouseover = "change(this, '#9966FF', '#330066')" 
	    on Mouseout = "change(this, '#8484ee', '#000033')">Help</a>
         </h4>
EOF
	 my $bugTarget = "BugDisplay.pl";
	 if($id != 0){
	    $bugTarget = "BugReportForm.pl?tool=Glycan Kernel Tool";
	 }
print <<EOF;
         <h4 style = "text-align : center;" align = "center">
            <a href = "/cgi-bin/tools/Bug/$bugTarget"
            onMouseover = "change(this, '#9966FF', '#330066')" on Mouseout = "change(this, '#8484ee', '#000033')">Feedback</a>
         </h4>
      </td>
   <td align = "center" width = 800><Font size = 4><B>
EOF

 print shift @_; #output error messages.
 print "<B></font></td></tr></table></body></html>";
 exit;
}




