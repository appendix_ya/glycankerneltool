#!/usr/bin/perl

use RINGS::Web::HTML;
use RINGS::Web::user_admin;

$id = Get_Cookie_Info();
$DBH = Connect_To_Rings();

print "Content-type: text/html\n\n";

#============
# Get User ID
#============

print <<EOF;
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta http-equiv="Content-Style-Type" content="text/css">
    <link rel="stylesheet" href="/rings2.css" type="text/css">
    <title>Glycan Kernel Tool</title>
</head>

<body>
<style>
   table.mytable { width: 100%; padding: 0px; solid #789DB3;}
   table.mytable th { font-size: 16px; border: 1px;
   vertical-align: middle; padding: 3px; font-weight: bold;}
   table.mytable td { font-size: 14px;
   vertical-align: middle; padding: 7px; }
   table.mytable tr.special td th tr { border-top: 1px solid #ff0000; border-bottom: 1px solid #ff0000; border-left: 1px solid #ff0000; border-right: 1px solid #ff0000; }
</style>

<div id = "menu">
   <p class="glow"><a href="/index.html"><img src="/rings4.png" border=0></a>
   <font size=6> Glycan Kernel Tool</font></p>
</div>


<hr>
<table width=1000 border=0 cellpadding=0 cellspacing=0>
   <tr valign="top">
      <td width=150>
         <h4 style="text-align : center;" align="center">
            <a href="/index.html" onMouseover="change(this, '#9966FF', '#330066')" onMouseout="change(this, '#8484ee', '#000033')">Home</a>
         </h4>
         <h4 style="text-align : center;" align="center">
            <a href="/help/help.html" onMouseover="change(this, '#9966FF', '#330066')" onMouseout="change(this, '#8484ee', '#000033')">Help</a>
         </h4>
         <h4 style="text-align : center;" align="center">
EOF
            my $bugTarget = "BugDisplay.pl";
            if($id != 0){
               $bugTarget = "BugReportForm.pl?tool=Glycan Kernel Tool";
            }
print <<EOF;
            <a href="/cgi-bin/tools/Bug/$bugTarget" onMouseover="change(this,'#9966FF','#000033')" onMouseout="change(this,'#8484ee','#000033')">Feedback</a>
         </h4>

		 <font size=3>Please input your kernel calculation ID to retrieve your previous result.<BR></font>
		 <form action="/cgi-bin/tools/Kernel/show-results.pl" method="POST" name="KERNEL" enctype="multipart/form-data">
		 <TABLE border = 1 cellpadding = 1 cellspacing = 0>
		 <TD align = "center">
		    <input type = "text" name = "id" value ="" size = "10">
		 </TD>
	  	 <TD colspan = "3" align = "center">
		    <input value="run" type="submit">
		 </TD>
	     </TABLE>
		 </form>

      </td>


      <form action="/cgi-bin/tools/Kernel/kernel-request.pl" method="POST" name="KERNEL" enctype="multipart/form-data">
      <td width=1000 border=10 cellpadding=0 cellspacing=0>
         <TABLE border = 3 cellpadding = 3 cellspacing = 0>
            <TBODY>
	       <TR>
                  <TD class = "bgc_glay"><B><FONT size = "3">Data set name</FONT></B>
                     <!--div><FONT size = "2">Input a datadata  name.</FONT></div-->
                  </TD>
                  <TD colspan = "2"><input type = "text" name = "classname" value = "Default" size = "50"></TD>
                  <tr>
                     <TD rowspan = "2" class = "bgc_glay"><B><FONT size = "3">Glycan Data</FONT></B>
                        <div><FONT size = "2">Enter KCF data consecutively.</FONT></div>
                     </TD>
                     <TD>
                        <font size=3><b>Target data&nbsp;<br></b></font>
			<textarea name ="target" rows="15" cols="25">
ENTRY	1024_3_1	Glycan
NODE	4
	1	gal	-8.0	-2.0
	2	glcnac	-8.0	2.0
	3	galnac	0.0	-0.0
	4	glcn	-14.0	2.0
EDGE	3
	1	1:b1	3:3
	2	2:b1	3:6
	3	4:b1	2:4
///
ENTRY	1140_4_2	Glycan
NODE	4
	1	neuac	-16.0	-2.0
	2	gal	-8.0	-2.0
	3	glcnac	-8.0	2.0
	4	galnac	0.0	0.0
EDGE	3
	1	1:a2	2:3
	2	2:b1	4:3
	3	3:b1	4:6
///
ENTRY	1188_4_2	Glycan
NODE	5
	1	gal	-8.0	-2.0
	2	gal	-16.0	2.0
	3	glcnac	-8.0	2.0
	4	galnac	0.0	-0.0
	5	gal	-15.0	-2.0
EDGE	4
	1	1:b1	4:3
	2	5:b1	1:3
	3	3:b1	4:6
	4	2:b1	3:4
///
ENTRY	1590_2	Glycan
NODE	7
	1	glcnac	-32.0	-6.0
	2	man	-24.0	-6.0
	3	man	-24.0	-2.0
	4	man	-16.0	-4.0
	5	glcnac	-8.0	-4.0
	6	fuc	-8.0	4.0
	7	glcnac	0.0	-0.0
EDGE	6
	1	5:b1	7:4
	2	4:b1	5:4
	3	2:a1	4:3
	4	1:b1	2:4
	5	3:a1	4:6
	6	6:a1	7:6
///
ENTRY	1661_3	Glycan
NODE	7
	1	glcnac	-32.0	-6.0
	2	man	-24.0	-6.0
	3	glcnac	-32.0	-2.0
	4	man	-24.0	-2.0
	5	man	-16.0	-4.0
	6	glcnac	-8.0	-4.0
	7	glcnac	0.0	-0.0
EDGE	6
	1	6:b1	7:4
	2	5:b1	6:4
	3	2:a1	5:3
	4	1:b1	2:4
	5	4:a1	5:6
	6	3:b1	4:2
///
ENTRY	1906_3	Glycan
NODE	8
	1	glcnac	-32.0	-6.0
	2	glcnac	-28.0	-0.0
	3	man	-24.0	-4.0
	4	glcnac	-32.0	4.0
	5	man	-24.0	4.0
	6	man	-16.0	-0.0
	7	glcnac	-8.0	-0.0
	8	glcnac	0.0	-0.0
EDGE	7
	1	7:b1	8:4
	2	6:b1	7:4
	3	3:a1	6:3
	4	1:b1	3:2
	5	5:a1	6:6
	6	4:b1	5:2
	7	2:a1	6:4
///
ENTRY	2400_4	Glycan
NODE	10
	1	neuac	-48.0	-6.0
	2	gal	-40.0	-6.0
	3	glcnac	-32.0	-6.0
	4	man	-24.0	-6.0
	5	glcnac	-32.0	-2.0
	6	man	-24.0	-2.0
	7	man	-16.0	-4.0
	8	glcnac	-8.0	-4.0
	9	fuc	-8.0	4.0
	10	glcnac	0.0	-0.0
EDGE	9
	1	8:b1	10:4
	2	7:b1	8:4
	3	4:a1	7:3
	4	3:b1	4:2
	5	2:b1	3:4
	6	1:a2	2:3
	7	6:a1	7:6
	8	5:b1	6:2
	9	9:a1	10:6
///
ENTRY	2604_4	Glycan
NODE	11
	1	neuac	-48.0	-10.0
	2	gal	-40.0	-10.0
	3	glcnac	-32.0	-10.0
	4	man	-24.0	-10.0
	5	gal	-40.0	-4.0
	6	glcnac	-32.0	-2.0
	7	man	-24.0	-2.0
	8	man	-16.0	-6.0
	9	glcnac	-8.0	-6.0
	10	fuc	-8.0	6.0
	11	glcnac	0.0	-0.0
EDGE	10
	1	9:b1	11:4
	2	8:b1	9:4
	3	4:a1	8:3
	4	3:b1	4:2
	5	2:b1	3:4
	6	1:a2	2:6
	7	7:a1	8:6
	8	6:b1	7:2
	9	5:b1	6:4
	10	10:a1	11:6
///
ENTRY	2778_5_2	Glycan
NODE	12
	1	neuac	-48.0	-10.0
	2	gal	-40.0	-10.0
	3	glcnac	-32.0	-10.0
	4	man	-24.0	-10.0
	5	gal	-40.0	-4.0
	6	glcnac	-32.0	-2.0
	7	man	-24.0	-2.0
	8	man	-16.0	-6.0
	9	glcnac	-8.0	-6.0
	10	fuc	-8.0	6.0
	11	glcnac	0.0	-0.0
	12	fuc	-34.0	-17.0
EDGE	11
	1	9:b1	11:4
	2	8:b1	9:4
	3	4:a1	8:3
	4	3:b1	4:2
	5	2:b1	3:4
	6	1:a2	2:6
	7	7:a1	8:6
	8	6:b1	7:2
	9	5:b1	6:4
	10	10:a1	11:6
	11	12:a1	3:3
///
ENTRY	2808_4	Glycan
NODE	12
	1	glcnac	-24.0	-10.0
	2	neuac	-48.0	-6.0
	3	gal	-40.0	-6.0
	4	glcnac	-32.0	-6.0
	5	man	-24.0	-6.0
	6	man	-24.0	-2.0
	7	man	-16.0	-6.0
	8	glcnac	-8.0	-6.0
	9	fuc	-8.0	6.0
	10	glcnac	0.0	-0.0
	11	man	-30.0	6.0
	12	man	-32.0	-0.0
EDGE	11
	1	8:b1	10:4
	2	7:b1	8:4
	3	1:b1	7:4
	4	5:a1	7:3
	5	4:b1	5:2
	6	3:b1	4:4
	7	2:a2	3:6
	8	6:a1	7:6
	9	9:a1	10:6
	10	11:a1	6:6
	11	12:a1	6:3
///
ENTRY	2820_5_2	Glycan
NODE	12
	1	glcnac	-24.0	-14.0
	2	neuac	-48.0	-10.0
	3	gal	-40.0	-10.0
	4	glcnac	-32.0	-10.0
	5	man	-24.0	-8.0
	6	glcnac	-32.0	-2.0
	7	man	-24.0	-2.0
	8	man	-16.0	-8.0
	9	glcnac	-8.0	-8.0
	10	fuc	-8.0	8.0
	11	glcnac	0.0	-0.0
	12	fuc	-33.0	-18.0
EDGE	11
	1	9:b1	11:4
	2	8:b1	9:4
	3	1:b1	8:4
	4	5:a1	8:3
	5	4:b1	5:2
	6	3:b1	4:4
	7	2:a2	3:3/6
	8	7:a1	8:6
	9	6:b1	7:2
	10	10:a1	11:6
	11	12:a1	4:3
///
ENTRY	2836_5	Glycan
NODE	13
	1	neuac	-48.0	-6.0
	2	gal	-40.0	-6.0
	3	glcnac	-32.0	-6.0
	4	man	-24.0	-6.0
	5	man	-24.0	-2.0
	6	man	-16.0	-6.0
	7	glcnac	-8.0	-6.0
	8	fuc	-8.0	6.0
	9	glcnac	0.0	-0.0
	10	man	-30.0	6.0
	11	man	-32.0	-0.0
	12	glcnac	-31.0	-11.0
	13	gal	-40.0	-11.0
EDGE	12
	1	7:b1	9:4
	2	6:b1	7:4
	3	4:a1	6:3
	4	3:b1	4:2
	5	2:b1	3:4
	6	1:a2	2:6
	7	5:a1	6:6
	8	8:a1	9:6
	9	10:a1	5:6
	10	11:a1	5:3
	11	12:b1	4:4
	12	13:b1	12:2
///
ENTRY	3023_5_2	Glycan
NODE	13
	1	glcnac	-24.0	-10.0
	2	neuac	-48.0	-6.0
	3	gal	-40.0	-6.0
	4	glcnac	-32.0	-6.0
	5	man	-24.0	-6.0
	6	gal	-40.0	-2.0
	7	glcnac	-32.0	-2.0
	8	man	-24.0	-2.0
	9	man	-16.0	-6.0
	10	glcnac	-8.0	-6.0
	11	fuc	-8.0	6.0
	12	glcnac	0.0	-0.0
	13	fuc	-35.0	-13.0
EDGE	12
	1	10:b1	12:4
	2	9:b1	10:4
	3	1:b1	9:4
	4	5:a1	9:3
	5	4:b1	5:2
	6	3:b1	4:4
	7	2:a2	3:6
	8	8:a1	9:6
	9	7:b1	8:2
	10	6:b1	7:4
	11	11:a1	12:6
	12	13:a1	4:3
///
ENTRY	3054_5	Glycan
NODE	13
	1	neuac	-48.0	-10.0
	2	gal	-40.0	-10.0
	3	glcnac	-32.0	-10.0
	4	man	-24.0	-10.0
	5	gal	-40.0	-4.0
	6	glcnac	-32.0	-2.0
	7	man	-24.0	-2.0
	8	man	-16.0	-6.0
	9	glcnac	-8.0	-6.0
	10	fuc	-8.0	6.0
	11	glcnac	0.0	-0.0
	12	glcnac	-32.0	-6.0
	13	gal	-40.0	-7.0
EDGE	12
	1	9:b1	11:4
	2	8:b1	9:4
	3	4:a1	8:3
	4	3:b1	4:2
	5	2:b1	3:4
	6	1:a2	2:6
	7	7:a1	8:6
	8	6:b1	7:2
	9	5:b1	6:4
	10	10:a1	11:6
	11	13:b1	12:2
	12	12:b1	4:4
///
ENTRY	3213_5	Glycan
NODE	13
	1	neuac	-48.0	-6.0
	2	gal	-40.0	-6.0
	3	glcnac	-32.0	-6.0
	4	man	-24.0	-6.0
	5	neuac	-48.0	-2.0
	6	gal	-40.0	-2.0
	7	glcnac	-32.0	-2.0
	8	man	-24.0	-2.0
	9	man	-16.0	-4.0
	10	glcnac	-8.0	-4.0
	11	fuc	-8.0	4.0
	12	glcnac	0.0	-0.0
	13	glcnac	-25.0	-4.0
EDGE	12
	1	10:b1	12:4
	2	9:b1	10:4
	3	4:a1	9:3
	4	3:b1	4:2
	5	2:b1	3:4
	6	1:a2	2:3
	7	8:a1	9:6
	8	7:b1	8:2
	9	6:b1	7:4
	10	5:a2	6:3
	11	11:a1	12:6
	12	13:b1	9:4
///
ENTRY	3268_6_2	Glycan
NODE	14
	1	glcnac	-24.0	-22.0
	2	neuac	-48.0	-16.0
	3	gal	-40.0	-16.0
	4	glcnac	-32.0	-16.0
	5	gal	-40.0	-10.0
	6	glcnac	-32.0	-8.0
	7	man	-24.0	-12.0
	8	glcnac	-32.0	-2.0
	9	man	-24.0	-2.0
	10	man	-16.0	-12.0
	11	glcnac	-8.0	-12.0
	12	fuc	-8.0	12.0
	13	glcnac	0.0	-0.0
	14	fuc	-36.0	-23.0
EDGE	13
	1	11:b1	13:4
	2	10:b1	11:4
	3	1:b1	10:4
	4	7:a1	10:3
	5	4:b1	7:2
	6	3:b1	4:4
	7	2:a2	3:3/6
	8	6:b1	7:4
	9	5:b1	6:4
	10	9:a1	10:6
	11	8:b1	9:2
	12	12:a1	13:6
	13	14:a1	4:3
///
ENTRY	3285_6	Glycan
NODE	15
	1	glcnac	-24.0	-26.0
	2	gal	-40.0	-22.0
	3	fuc	-40.0	-18.0
	4	glcnac	-32.0	-20.0
	5	gal	-40.0	-10.0
	6	fuc	-40.0	-6.0
	7	glcnac	-32.0	-8.0
	8	man	-24.0	-14.0
	9	gal	-40.0	-4.0
	10	glcnac	-32.0	-2.0
	11	man	-24.0	-2.0
	12	man	-16.0	-14.0
	13	glcnac	-8.0	-14.0
	14	fuc	-8.0	14.0
	15	glcnac	0.0	-0.0
EDGE	14
	1	13:b1	15:4
	2	12:b1	13:4
	3	1:b1	12:4
	4	8:a1	12:3
	5	4:b1	8:2
	6	2:b1	4:4
	7	3:a1	4:3
	8	7:b1	8:4
	9	5:b1	7:4
	10	6:a1	7:3
	11	11:a1	12:6
	12	10:b1	11:2
	13	9:b1	10:4
	14	14:a1	15:6
///
ENTRY	3326_7	Glycan
NODE	14
	1	neuac	-48.0	-20.0
	2	gal	-40.0	-20.0
	3	glcnac	-32.0	-20.0
	4	gal	-40.0	-16.0
	5	glcnac	-32.0	-16.0
	6	man	-24.0	-18.0
	7	gal	-40.0	-6.0
	8	glcnac	-32.0	-6.0
	9	gal	-40.0	-0.0
	10	glcnac	-32.0	2.0
	11	man	-24.0	-2.0
	12	man	-16.0	-10.0
	13	glcnac	-8.0	-10.0
	14	glcnac	0.0	-0.0
EDGE	13
	1	13:b1	14:4
	2	12:b1	13:4
	3	6:a1	12:3
	4	3:b1	6:2
	5	2:b1	3:4
	6	1:a2	2:3/6
	7	5:b1	6:4
	8	4:b1	5:4
	9	11:a1	12:6
	10	8:b1	11:2
	11	7:b1	8:4
	12	10:b1	11:6
	13	9:b1	10:4
///
ENTRY	3384_4	Glycan
NODE	14
	1	gal	-40.0	-20.0
	2	fuc	-40.0	-16.0
	3	glcnac	-32.0	-18.0
	4	man	-24.0	-18.0
	5	neuac	-48.0	-6.0
	6	gal	-40.0	-6.0
	7	glcnac	-32.0	-6.0
	8	man	-24.0	-2.0
	9	man	-16.0	-10.0
	10	glcnac	-8.0	-10.0
	11	fuc	-8.0	10.0
	12	glcnac	0.0	-0.0
	13	neuac	-48.0	-21.0
	14	glcnac	-25.0	-10.0
EDGE	13
	1	10:b1	12:4
	2	9:b1	10:4
	3	4:a1	9:3
	4	3:b1	4:2
	5	1:b1	3:4
	6	2:a1	3:3
	7	8:a1	9:6
	8	7:b1	8:2
	9	6:b1	7:4
	10	5:a2	6:3/6
	11	11:a1	12:6
	12	13:a2	1:3/6
	13	14:b1	9:4
///
ENTRY	3400_6_3	Glycan
NODE	15
	1	neuac	-48.0	-16.0
	2	gal	-40.0	-16.0
	3	glcnac	-32.0	-16.0
	4	gal	-40.0	-10.0
	5	glcnac	-32.0	-8.0
	6	man	-24.0	-12.0
	7	gal	-40.0	-4.0
	8	glcnac	-32.0	-2.0
	9	man	-24.0	-2.0
	10	man	-16.0	-12.0
	11	glcnac	-8.0	-12.0
	12	fuc	-8.0	12.0
	13	glcnac	0.0	-0.0
	14	fuc	-35.0	-23.0
	15	fuc	-36.0	-13.0
EDGE	14
	1	11:b1	13:4
	2	10:b1	11:4
	3	6:a1	10:3
	4	3:b1	6:2
	5	2:b1	3:4
	6	1:a2	2:3/6
	7	5:b1	6:4
	8	4:b1	5:4
	9	9:a1	10:6
	10	8:b1	9:2
	11	7:b1	8:4
	12	12:a1	13:6
	13	14:a1	3:3
	14	15:a1	5:3
///
ENTRY	3414_5	Glycan
NODE	14
	1	neuac	-48.0	-20.0
	2	gal	-40.0	-20.0
	3	glcnac	-32.0	-20.0
	4	neuac	-48.0	-16.0
	5	gal	-40.0	-16.0
	6	glcnac	-32.0	-16.0
	7	man	-24.0	-18.0
	8	gal	-40.0	-6.0
	9	glcnac	-32.0	-6.0
	10	man	-24.0	-2.0
	11	man	-16.0	-10.0
	12	glcnac	-8.0	-10.0
	13	fuc	-8.0	10.0
	14	glcnac	0.0	-0.0
EDGE	13
	1	12:b1	14:4
	2	11:b1	12:4
	3	7:a1	11:3
	4	3:b1	7:2
	5	2:b1	3:4
	6	1:a2	2:3/6
	7	6:b1	7:4
	8	5:b1	6:4
	9	4:a2	5:3/6
	10	10:a1	11:6
	11	9:b1	10:2
	12	8:b1	9:4
	13	13:a1	14:6
///
ENTRY	3440_5	Glycan
NODE	15
	1	glcnac	-24.0	-22.0
	2	neuac	-48.0	-16.0
	3	gal	-40.0	-16.0
	4	glcnac	-32.0	-16.0
	5	gal	-40.0	-10.0
	6	fuc	-40.0	-6.0
	7	glcnac	-32.0	-8.0
	8	man	-24.0	-12.0
	9	glcnac	-32.0	-2.0
	10	man	-24.0	-2.0
	11	man	-16.0	-12.0
	12	glcnac	-8.0	-12.0
	13	fuc	-8.0	12.0
	14	glcnac	0.0	-0.0
	15	fuc	-39.0	-21.0
EDGE	14
	1	12:b1	14:4
	2	11:b1	12:4
	3	1:b1	11:4
	4	8:a1	11:3
	5	4:b1	8:2
	6	3:b1	4:4
	7	2:a2	3:3/6
	8	7:b1	8:4
	9	5:b1	7:4
	10	6:a1	7:3
	11	10:a1	11:6
	12	9:b1	10:2
	13	13:a1	14:6
	14	15:a1	4:3
///
ENTRY	3470_4_3	Glycan
NODE	15
	1	neuac	-48.0	-16.0
	2	gal	-40.0	-16.0
	3	glcnac	-32.0	-16.0
	4	gal	-40.0	-10.0
	5	glcnac	-32.0	-8.0
	6	man	-24.0	-12.0
	7	gal	-40.0	-4.0
	8	glcnac	-32.0	-2.0
	9	man	-24.0	-2.0
	10	man	-16.0	-12.0
	11	glcnac	-8.0	-12.0
	12	fuc	-8.0	12.0
	13	glcnac	0.0	-0.0
	14	glcnac	-22.0	-19.0
	15	fuc	-38.0	-22.0
EDGE	14
	1	11:b1	13:4
	2	10:b1	11:4
	3	6:a1	10:3
	4	3:b1	6:2
	5	2:b1	3:4
	6	1:a2	2:3/6
	7	5:b1	6:4
	8	4:b1	5:4
	9	9:a1	10:6
	10	8:b1	9:2
	11	7:b1	8:4
	12	14:b1	10:4
	13	12:a1	13:6
	14	15:a1	3:3
///
ENTRY	3512_6_4	Glycan
NODE	15
	1	glcnac	-24.0	-22.0
	2	glcnac	-33.0	1.0
	3	glcnac	-33.0	7.0
	4	man	-24.0	-1.0
	5	neuac	-48.0	-13.0
	6	gal	-41.0	-10.0
	7	glcnac	-33.0	-11.0
	8	gal	-42.0	-2.0
	9	glcnac	-33.0	-4.0
	10	man	-24.0	-12.0
	11	man	-16.0	-12.0
	12	glcnac	-8.0	-12.0
	13	fuc	-8.0	12.0
	14	glcnac	0.0	-0.0
	15	fuc	-40.0	-19.0
EDGE	14
	1	12:b1	14:4
	2	11:b1	12:4
	3	1:b1	11:4
	4	4:a1	11:3
	5	2:b1	4:2
	6	3:b1	4:6
	7	10:a1	11:6
	8	7:b1	10:2
	9	6:b1	7:4
	10	5:a2	6:3/6
	11	15:a1	7:3
	12	9:b1	10:4
	13	8:b1	9:4
	14	13:a1	14:6
///
ENTRY	3588_6_3	Glycan
NODE	15
	1	neuac	-48.0	-20.0
	2	gal	-40.0	-20.0
	3	glcnac	-32.0	-20.0
	4	neuac	-48.0	-16.0
	5	gal	-40.0	-16.0
	6	glcnac	-32.0	-16.0
	7	man	-24.0	-18.0
	8	gal	-40.0	-6.0
	9	glcnac	-32.0	-6.0
	10	man	-24.0	-2.0
	11	man	-16.0	-10.0
	12	glcnac	-8.0	-10.0
	13	fuc	-8.0	10.0
	14	glcnac	0.0	-0.0
	15	fuc	-37.0	-24.0
EDGE	14
	1	12:b1	14:4
	2	11:b1	12:4
	3	7:a1	11:3
	4	3:b1	7:2
	5	2:b1	3:4
	6	1:a2	2:3/6
	7	6:b1	7:4
	8	5:b1	6:4
	9	4:a2	5:3/6
	10	10:a1	11:6
	11	9:b1	10:2
	12	8:b1	9:4
	13	13:a1	14:6
	14	15:a1	3:3
///
ENTRY	3629_7	Glycan
NODE	15
	1	glcnac	-24.0	-22.0
	2	neuac	-48.0	-16.0
	3	gal	-40.0	-16.0
	4	glcnac	-32.0	-16.0
	5	gal	-40.0	-10.0
	6	glcnac	-32.0	-8.0
	7	man	-24.0	-12.0
	8	glcnac	-32.0	-2.0
	9	man	-24.0	-2.0
	10	man	-16.0	-12.0
	11	glcnac	-8.0	-12.0
	12	fuc	-8.0	12.0
	13	glcnac	0.0	-0.0
	14	neuac	-48.0	-10.0
	15	fuc	-40.0	-22.0
EDGE	14
	1	11:b1	13:4
	2	10:b1	11:4
	3	1:b1	10:4
	4	7:a1	10:3
	5	4:b1	7:2
	6	3:b1	4:4
	7	2:a2	3:3/6
	8	6:b1	7:4
	9	5:b1	6:4
	10	14:a2	5:3/6
	11	9:a1	10:6
	12	8:b1	9:2
	13	12:a1	13:6
	14	15:a1	4:3
///
ENTRY	3646_6_3	Glycan
NODE	16
	1	neuac	-48.0	-16.0
	2	gal	-40.0	-16.0
	3	glcnac	-32.0	-16.0
	4	gal	-40.0	-10.0
	5	glcnac	-32.0	-8.0
	6	man	-24.0	-12.0
	7	gal	-40.0	-4.0
	8	glcnac	-32.0	-2.0
	9	man	-24.0	-2.0
	10	man	-16.0	-12.0
	11	glcnac	-8.0	-12.0
	12	fuc	-8.0	12.0
	13	glcnac	0.0	-0.0
	14	glcnac	-22.0	-19.0
	15	fuc	-38.0	-22.0
	16	fuc	-37.0	-5.0
EDGE	15
	1	11:b1	13:4
	2	10:b1	11:4
	3	6:a1	10:3
	4	3:b1	6:2
	5	2:b1	3:4
	6	1:a2	2:3/6
	7	5:b1	6:4
	8	4:b1	5:4
	9	9:a1	10:6
	10	8:b1	9:2
	11	7:b1	8:4
	12	14:b1	10:4
	13	12:a1	13:6
	14	15:a1	3:3
	15	16:a1	5:3
///
ENTRY	3659_6	Glycan
NODE	15
	1	glcnac	-24.0	-22.0
	2	neuac	-48.0	-16.0
	3	gal	-40.0	-16.0
	4	glcnac	-32.0	-16.0
	5	gal	-40.0	-10.0
	6	glcnac	-32.0	-8.0
	7	man	-24.0	-12.0
	8	glcnac	-32.0	-2.0
	9	man	-24.0	-2.0
	10	man	-16.0	-12.0
	11	glcnac	-8.0	-12.0
	12	fuc	-8.0	12.0
	13	glcnac	0.0	-0.0
	14	neuac	-48.0	-10.0
	15	gal	-41.0	-0.0
EDGE	14
	1	11:b1	13:4
	2	10:b1	11:4
	3	1:b1	10:4
	4	7:a1	10:3
	5	4:b1	7:2
	6	3:b1	4:4
	7	2:a2	3:3/6
	8	6:b1	7:4
	9	5:b1	6:4
	10	14:a2	5:3/6
	11	9:a1	10:6
	12	8:b1	9:2
	13	12:a1	13:6
	14	15:b1	8:4
///
ENTRY	3687_5	Glycan
NODE	15
	1	neuac	-48.0	-20.0
	2	gal	-40.0	-20.0
	3	glcnac	-32.0	-20.0
	4	neuac	-48.0	-16.0
	5	gal	-40.0	-16.0
	6	glcnac	-32.0	-16.0
	7	man	-24.0	-18.0
	8	gal	-40.0	-6.0
	9	glcnac	-32.0	-6.0
	10	gal	-40.0	-0.0
	11	glcnac	-32.0	2.0
	12	man	-24.0	-2.0
	13	man	-16.0	-10.0
	14	glcnac	-8.0	-10.0
	15	glcnac	0.0	-0.0
EDGE	14
	1	14:b1	15:4
	2	13:b1	14:4
	3	7:a1	13:3
	4	3:b1	7:2
	5	2:b1	3:4
	6	1:a2	2:3/6
	7	6:b1	7:4
	8	5:b1	6:4
	9	4:a2	5:3/6
	10	12:a1	13:6
	11	9:b1	12:2
	12	8:b1	9:4
	13	11:b1	12:6
	14	10:b1	11:4
///
ENTRY	3699_4	Glycan
NODE	15
	1	glcnac	-24.0	-22.0
	2	neuac	-48.0	-16.0
	3	gal	-40.0	-16.0
	4	glcnac	-32.0	-16.0
	5	gal	-40.0	-10.0
	6	glcnac	-32.0	-8.0
	7	man	-24.0	-12.0
	8	glcnac	-32.0	-2.0
	9	man	-24.0	-2.0
	10	man	-16.0	-12.0
	11	glcnac	-8.0	-12.0
	12	fuc	-8.0	12.0
	13	glcnac	0.0	-0.0
	14	neuac	-48.0	-10.0
	15	glcnac	-33.0	6.0
EDGE	14
	1	11:b1	13:4
	2	10:b1	11:4
	3	1:b1	10:4
	4	7:a1	10:3
	5	4:b1	7:2
	6	3:b1	4:4
	7	2:a2	3:3/6
	8	6:b1	7:4
	9	5:b1	6:4
	10	14:a2	5:3/6
	11	9:a1	10:6
	12	8:b1	9:2
	13	15:b1	9:6
	14	12:a1	13:6
///
ENTRY	3774_5	Glycan
NODE	15
	1	neuac	-48.0	-20.0
	2	gal	-40.0	-20.0
	3	glcnac	-32.0	-20.0
	4	neuac	-48.0	-16.0
	5	gal	-40.0	-16.0
	6	glcnac	-32.0	-16.0
	7	man	-24.0	-18.0
	8	neuac	-48.0	-6.0
	9	gal	-40.0	-6.0
	10	glcnac	-32.0	-6.0
	11	man	-24.0	-2.0
	12	man	-16.0	-10.0
	13	glcnac	-8.0	-10.0
	14	fuc	-8.0	10.0
	15	glcnac	0.0	-0.0
EDGE	14
	1	13:b1	15:4
	2	12:b1	13:4
	3	7:a1	12:3
	4	3:b1	7:2
	5	2:b1	3:4
	6	1:a2	2:3/6
	7	6:b1	7:4
	8	5:b1	6:4
	9	4:a2	5:3/6
	10	11:a1	12:6
	11	10:b1	11:2
	12	9:b1	10:4
	13	8:a2	9:3/6
	14	14:a1	15:6
///
ENTRY	3833_6_3	Glycan
NODE	16
	1	glcnac	-24.0	-22.0
	2	neuac	-48.0	-16.0
	3	gal	-40.0	-16.0
	4	glcnac	-32.0	-16.0
	5	gal	-40.0	-10.0
	6	glcnac	-32.0	-8.0
	7	man	-24.0	-12.0
	8	glcnac	-32.0	-2.0
	9	man	-24.0	-2.0
	10	man	-16.0	-12.0
	11	glcnac	-8.0	-12.0
	12	fuc	-8.0	12.0
	13	glcnac	0.0	-0.0
	14	neuac	-48.0	-10.0
	15	gal	-41.0	-0.0
	16	fuc	-39.0	-22.0
EDGE	15
	1	11:b1	13:4
	2	10:b1	11:4
	3	1:b1	10:4
	4	7:a1	10:3
	5	4:b1	7:2
	6	3:b1	4:4
	7	2:a2	3:3/6
	8	6:b1	7:4
	9	5:b1	6:4
	10	14:a2	5:3/6
	11	9:a1	10:6
	12	8:b1	9:2
	13	15:b1	8:4
	14	12:a1	13:6
	15	16:a1	4:3
///
ENTRY	3850_6_6	Glycan
NODE	17
	1	neuac	-48.0	-20.0
	2	gal	-40.0	-20.0
	3	glcnac	-32.0	-20.0
	4	gal	-40.0	-16.0
	5	glcnac	-32.0	-16.0
	6	man	-24.0	-18.0
	7	gal	-40.0	-6.0
	8	glcnac	-32.0	-6.0
	9	gal	-40.0	-0.0
	10	glcnac	-32.0	2.0
	11	man	-24.0	-2.0
	12	man	-16.0	-10.0
	13	glcnac	-8.0	-10.0
	14	glcnac	0.0	-0.0
	15	fuc	-10.0	6.0
	16	fuc	-39.0	-23.0
	17	fuc	-38.0	-10.0
EDGE	16
	1	13:b1	14:4
	2	12:b1	13:4
	3	6:a1	12:3
	4	3:b1	6:2
	5	2:b1	3:4
	6	1:a2	2:3/6
	7	5:b1	6:4
	8	4:b1	5:4
	9	11:a1	12:6
	10	8:b1	11:2
	11	7:b1	8:4
	12	10:b1	11:6
	13	9:b1	10:4
	14	15:a1	14:6
	15	16:a1	3:3
	16	17:a1	5:3
///
ENTRY	3890_5_6	Glycan
NODE	17
	1	glcnac	-24.0	-22.0
	2	neuac	-48.0	-16.0
	3	gal	-40.0	-16.0
	4	glcnac	-32.0	-16.0
	5	gal	-40.0	-10.0
	6	glcnac	-32.0	-8.0
	7	man	-24.0	-12.0
	8	glcnac	-32.0	-2.0
	9	man	-24.0	-2.0
	10	man	-16.0	-12.0
	11	glcnac	-8.0	-12.0
	12	fuc	-8.0	12.0
	13	glcnac	0.0	-0.0
	14	glcnac	-33.0	6.0
	15	gal	-40.0	-2.0
	16	fuc	-40.0	-21.0
	17	fuc	-43.0	-4.0
EDGE	16
	1	11:b1	13:4
	2	10:b1	11:4
	3	1:b1	10:4
	4	7:a1	10:3
	5	4:b1	7:2
	6	3:b1	4:4
	7	2:a2	3:3/6
	8	16:a1	4:3
	9	6:b1	7:4
	10	5:b1	6:4
	11	17:a1	6:3
	12	9:a1	10:6
	13	8:b1	9:2
	14	15:b1	8:4
	15	14:b1	9:6
	16	12:a1	13:6
///
ENTRY	3949_6	Glycan
NODE	16
	1	neuac	-48.0	-20.0
	2	gal	-40.0	-20.0
	3	glcnac	-32.0	-20.0
	4	neuac	-48.0	-16.0
	5	gal	-40.0	-16.0
	6	glcnac	-32.0	-16.0
	7	man	-24.0	-18.0
	8	gal	-40.0	-6.0
	9	glcnac	-32.0	-6.0
	10	man	-24.0	-2.0
	11	man	-16.0	-10.0
	12	glcnac	-8.0	-10.0
	13	fuc	-8.0	10.0
	14	glcnac	0.0	-0.0
	15	fuc	-39.0	-25.0
	16	neuac	-48.0	-7.0
EDGE	15
	1	12:b1	14:4
	2	11:b1	12:4
	3	7:a1	11:3
	4	3:b1	7:2
	5	2:b1	3:4
	6	1:a2	2:3/6
	7	6:b1	7:4
	8	5:b1	6:4
	9	4:a2	5:3/6
	10	10:a1	11:6
	11	9:b1	10:2
	12	8:b1	9:4
	13	13:a1	14:6
	14	15:a1	3:3
	15	16:a2	8:3/6
///
ENTRY	4006_7_3	Glycan
NODE	17
	1	glcnac	-24.0	-22.0
	2	neuac	-48.0	-16.0
	3	gal	-40.0	-16.0
	4	glcnac	-32.0	-16.0
	5	gal	-40.0	-10.0
	6	glcnac	-32.0	-8.0
	7	man	-24.0	-12.0
	8	glcnac	-32.0	-2.0
	9	man	-24.0	-2.0
	10	man	-16.0	-12.0
	11	glcnac	-8.0	-12.0
	12	fuc	-8.0	12.0
	13	glcnac	0.0	-0.0
	14	neuac	-48.0	-10.0
	15	gal	-41.0	-0.0
	16	fuc	-39.0	-21.0
	17	fuc	-40.0	-3.0
EDGE	16
	1	11:b1	13:4
	2	10:b1	11:4
	3	1:b1	10:4
	4	7:a1	10:3
	5	4:b1	7:2
	6	3:b1	4:4
	7	2:a2	3:3/6
	8	6:b1	7:4
	9	5:b1	6:4
	10	14:a2	5:3/6
	11	9:a1	10:6
	12	8:b1	9:2
	13	15:b1	8:4
	14	12:a1	13:6
	15	16:a1	4:3
	16	17:a1	6:3
///
ENTRY	4020_4	Glycan
NODE	16
	1	neuac	-48.0	-16.0
	2	gal	-40.0	-16.0
	3	glcnac	-32.0	-16.0
	4	gal	-40.0	-10.0
	5	glcnac	-32.0	-8.0
	6	man	-24.0	-12.0
	7	gal	-40.0	-4.0
	8	glcnac	-32.0	-2.0
	9	man	-24.0	-2.0
	10	man	-16.0	-12.0
	11	glcnac	-8.0	-12.0
	12	fuc	-8.0	12.0
	13	glcnac	0.0	-0.0
	14	glcnac	-22.0	-19.0
	15	neuac	-50.0	-9.0
	16	neuac	-51.0	-4.0
EDGE	15
	1	11:b1	13:4
	2	10:b1	11:4
	3	6:a1	10:3
	4	3:b1	6:2
	5	2:b1	3:4
	6	1:a2	2:3/6
	7	5:b1	6:4
	8	4:b1	5:4
	9	9:a1	10:6
	10	8:b1	9:2
	11	7:b1	8:4
	12	14:b1	10:4
	13	12:a1	13:6
	14	15:a2	4:3/6
	15	16:a2	7:3/6
///
ENTRY	4078_8_4	Glycan
NODE	17
	1	glcnac	-24.0	-22.0
	2	neuac	-48.0	-16.0
	3	gal	-40.0	-16.0
	4	glcnac	-32.0	-16.0
	5	gal	-40.0	-10.0
	6	glcnac	-32.0	-8.0
	7	man	-24.0	-12.0
	8	glcnac	-32.0	-2.0
	9	man	-24.0	-2.0
	10	man	-16.0	-12.0
	11	glcnac	-8.0	-12.0
	12	fuc	-8.0	12.0
	13	glcnac	0.0	-0.0
	14	glcnac	-33.0	6.0
	15	gal	-40.0	-2.0
	16	neuac	-49.0	-10.0
	17	fuc	-40.0	-20.0
EDGE	16
	1	11:b1	13:4
	2	10:b1	11:4
	3	1:b1	10:4
	4	7:a1	10:3
	5	4:b1	7:2
	6	3:b1	4:4
	7	2:a2	3:3/6
	8	17:a1	4:3
	9	6:b1	7:4
	10	5:b1	6:4
	11	16:a2	5:3/6
	12	9:a1	10:6
	13	8:b1	9:2
	14	15:b1	8:4
	15	14:b1	9:6
	16	12:a1	13:6
///
ENTRY	4136_5_12	Glycan
NODE	17
	1	gal	-40.0	-20.0
	2	glcnac	-32.0	-20.0
	3	gal	-40.0	-16.0
	4	glcnac	-32.0	-16.0
	5	man	-24.0	-18.0
	6	gal	-40.0	-6.0
	7	glcnac	-32.0	-6.0
	8	gal	-40.0	-0.0
	9	glcnac	-32.0	2.0
	10	man	-24.0	-2.0
	11	man	-16.0	-10.0
	12	glcnac	-8.0	-10.0
	13	glcnac	0.0	-0.0
	14	glcnac	-48.0	8.0
	15	gal	-57.0	10.0
	16	neuac	-52.0	-14.0
	17	neuac	-51.0	-2.0
EDGE	16
	1	12:b1	13:4
	2	11:b1	12:4
	3	5:a1	11:3
	4	2:b1	5:2
	5	1:b1	2:4
	6	16:a2	1:3/6
	7	4:b1	5:4
	8	3:b1	4:4
	9	17:a2	3:3/6
	10	10:a1	11:6
	11	7:b1	10:2
	12	6:b1	7:4
	13	14:b1	6:3
	14	15:b1	14:4
	15	9:b1	10:6
	16	8:b1	9:4
///
ENTRY	4192_5_3	Glycan
NODE	17
	1	neuac	-48.0	-16.0
	2	gal	-40.0	-16.0
	3	glcnac	-32.0	-16.0
	4	gal	-40.0	-10.0
	5	glcnac	-32.0	-8.0
	6	man	-24.0	-12.0
	7	gal	-40.0	-4.0
	8	glcnac	-32.0	-2.0
	9	man	-24.0	-2.0
	10	man	-16.0	-12.0
	11	glcnac	-8.0	-12.0
	12	fuc	-8.0	12.0
	13	glcnac	0.0	-0.0
	14	glcnac	-22.0	-19.0
	15	neuac	-50.0	-9.0
	16	neuac	-51.0	-4.0
	17	fuc	-40.0	-21.0
EDGE	16
	1	11:b1	13:4
	2	10:b1	11:4
	3	6:a1	10:3
	4	3:b1	6:2
	5	2:b1	3:4
	6	1:a2	2:3/6
	7	5:b1	6:4
	8	4:b1	5:4
	9	15:a2	4:3/6
	10	9:a1	10:6
	11	8:b1	9:2
	12	7:b1	8:4
	13	16:a2	7:3/6
	14	14:b1	10:4
	15	12:a1	13:6
	16	17:a1	3:3
///
ENTRY	4211_6_6	Glycan
NODE	18
	1	neuac	-48.0	-20.0
	2	gal	-40.0	-20.0
	3	glcnac	-32.0	-20.0
	4	neuac	-48.0	-16.0
	5	gal	-40.0	-16.0
	6	glcnac	-32.0	-16.0
	7	man	-24.0	-18.0
	8	gal	-40.0	-6.0
	9	glcnac	-32.0	-6.0
	10	gal	-40.0	-0.0
	11	glcnac	-32.0	2.0
	12	man	-24.0	-2.0
	13	man	-16.0	-10.0
	14	glcnac	-8.0	-10.0
	15	glcnac	0.0	-0.0
	16	fuc	-16.0	6.0
	17	fuc	-41.0	-24.0
	18	fuc	-40.0	-11.0
EDGE	17
	1	14:b1	15:4
	2	13:b1	14:4
	3	7:a1	13:3
	4	3:b1	7:2
	5	2:b1	3:4
	6	1:a2	2:3/6
	7	6:b1	7:4
	8	5:b1	6:4
	9	4:a2	5:3/6
	10	12:a1	13:6
	11	9:b1	12:2
	12	8:b1	9:4
	13	11:b1	12:6
	14	10:b1	11:4
	15	16:a1	15:6
	16	17:a1	3:3
	17	18:a1	6:3
///
ENTRY	4265_6	Glycan
NODE	17
	1	neuac	-48.0	-16.0
	2	gal	-40.0	-16.0
	3	glcnac	-32.0	-16.0
	4	gal	-40.0	-10.0
	5	glcnac	-32.0	-8.0
	6	man	-24.0	-12.0
	7	gal	-40.0	-4.0
	8	glcnac	-32.0	-2.0
	9	man	-24.0	-2.0
	10	man	-16.0	-12.0
	11	glcnac	-8.0	-12.0
	12	fuc	-8.0	12.0
	13	glcnac	0.0	-0.0
	14	glcnac	-22.0	-19.0
	15	neuac	-50.0	-9.0
	16	neuac	-51.0	-4.0
	17	glcnac	-33.0	5.0
EDGE	16
	1	11:b1	13:4
	2	10:b1	11:4
	3	6:a1	10:3
	4	3:b1	6:2
	5	2:b1	3:4
	6	1:a2	2:3/6
	7	5:b1	6:4
	8	4:b1	5:4
	9	15:a2	4:3/6
	10	9:a1	10:6
	11	8:b1	9:2
	12	7:b1	8:4
	13	16:a2	7:3/6
	14	17:b1	9:6
	15	14:b1	10:4
	16	12:a1	13:6
///
ENTRY	4397_3_4	Glycan
NODE	18
	1	neuac	-48.0	-20.0
	2	gal	-40.0	-20.0
	3	glcnac	-32.0	-20.0
	4	neuac	-48.0	-16.0
	5	gal	-40.0	-16.0
	6	glcnac	-32.0	-16.0
	7	man	-24.0	-18.0
	8	gal	-40.0	-6.0
	9	glcnac	-32.0	-6.0
	10	gal	-40.0	-0.0
	11	glcnac	-32.0	2.0
	12	man	-24.0	-2.0
	13	man	-16.0	-10.0
	14	glcnac	-8.0	-10.0
	15	glcnac	0.0	-0.0
	16	fuc	-16.0	6.0
	17	neuac	-49.0	-5.0
	18	fuc	-40.0	-24.0
EDGE	17
	1	14:b1	15:4
	2	13:b1	14:4
	3	7:a1	13:3
	4	3:b1	7:2
	5	2:b1	3:4
	6	1:a2	2:3/6
	7	6:b1	7:4
	8	5:b1	6:4
	9	4:a2	5:3/6
	10	12:a1	13:6
	11	9:b1	12:2
	12	8:b1	9:4
	13	17:a2	8:3/6
	14	11:b1	12:6
	15	10:b1	11:4
	16	16:a1	15:6
	17	18:a1	3:3
///
ENTRY	4571_7_6	Glycan
NODE	19
	1	neuac	-48.0	-20.0
	2	gal	-40.0	-20.0
	3	glcnac	-32.0	-20.0
	4	neuac	-48.0	-16.0
	5	gal	-40.0	-16.0
	6	glcnac	-32.0	-16.0
	7	man	-24.0	-18.0
	8	gal	-40.0	-6.0
	9	glcnac	-32.0	-6.0
	10	gal	-40.0	-0.0
	11	glcnac	-32.0	2.0
	12	man	-24.0	-2.0
	13	man	-16.0	-10.0
	14	glcnac	-8.0	-10.0
	15	glcnac	0.0	-0.0
	16	fuc	-16.0	6.0
	17	neuac	-49.0	-5.0
	18	fuc	-40.0	-9.0
	19	fuc	-40.0	-25.0
EDGE	18
	1	14:b1	15:4
	2	13:b1	14:4
	3	7:a1	13:3
	4	3:b1	7:2
	5	2:b1	3:4
	6	1:a2	2:3/6
	7	6:b1	7:4
	8	5:b1	6:4
	9	4:a2	5:3/6
	10	12:a1	13:6
	11	9:b1	12:2
	12	8:b1	9:4
	13	17:a2	8:3/6
	14	11:b1	12:6
	15	10:b1	11:4
	16	16:a1	15:6
	17	19:a1	3:3
	18	18:a1	6:3
///
ENTRY	4583_3_6	Glycan
NODE	19
	1	gal	-40.0	-20.0
	2	glcnac	-32.0	-20.0
	3	gal	-40.0	-16.0
	4	glcnac	-32.0	-16.0
	5	man	-24.0	-18.0
	6	gal	-40.0	-6.0
	7	glcnac	-32.0	-6.0
	8	gal	-40.0	-0.0
	9	glcnac	-32.0	2.0
	10	man	-24.0	-2.0
	11	man	-16.0	-10.0
	12	glcnac	-8.0	-10.0
	13	glcnac	0.0	-0.0
	14	glcnac	-49.0	-10.0
	15	gal	-57.0	-10.0
	16	glcnac	-49.0	-5.0
	17	gal	-57.0	-4.0
	18	neuac	-49.0	-14.0
	19	neuac	-50.0	-18.0
EDGE	18
	1	12:b1	13:4
	2	11:b1	12:4
	3	5:a1	11:3
	4	2:b1	5:2
	5	1:b1	2:4
	6	19:a2	1:3/6
	7	4:b1	5:4
	8	3:b1	4:4
	9	18:a2	3:3/6
	10	10:a1	11:6
	11	7:b1	10:2
	12	6:b1	7:4
	13	14:b1	6:3
	14	15:b1	14:4
	15	9:b1	10:6
	16	8:b1	9:4
	17	16:b1	8:3
	18	17:b1	16:4
///
ENTRY	4644_4_4	Glycan
NODE	19
	1	neuac	-48.0	-16.0
	2	gal	-40.0	-16.0
	3	glcnac	-32.0	-16.0
	4	gal	-40.0	-10.0
	5	glcnac	-32.0	-8.0
	6	man	-24.0	-12.0
	7	gal	-40.0	-4.0
	8	glcnac	-32.0	-2.0
	9	man	-24.0	-2.0
	10	man	-16.0	-12.0
	11	glcnac	-8.0	-12.0
	12	fuc	-8.0	12.0
	13	glcnac	0.0	-0.0
	14	glcnac	-22.0	-19.0
	15	neuac	-50.0	-9.0
	16	neuac	-51.0	-4.0
	17	glcnac	-33.0	5.0
	18	gal	-42.0	6.0
	19	fuc	-40.0	-21.0
EDGE	18
	1	11:b1	13:4
	2	10:b1	11:4
	3	6:a1	10:3
	4	3:b1	6:2
	5	2:b1	3:4
	6	1:a2	2:3/6
	7	19:a1	3:3
	8	5:b1	6:4
	9	4:b1	5:4
	10	15:a2	4:3/6
	11	9:a1	10:6
	12	8:b1	9:2
	13	7:b1	8:4
	14	16:a2	7:3/6
	15	17:b1	9:6
	16	18:b1	17:4
	17	14:b1	10:4
	18	12:a1	13:6
///
ENTRY	4758_4	Glycan
NODE	19
	1	neuac	-48.0	-20.0
	2	gal	-40.0	-20.0
	3	glcnac	-32.0	-20.0
	4	neuac	-48.0	-16.0
	5	gal	-40.0	-16.0
	6	glcnac	-32.0	-16.0
	7	man	-24.0	-18.0
	8	gal	-40.0	-6.0
	9	glcnac	-32.0	-6.0
	10	gal	-40.0	-0.0
	11	glcnac	-32.0	2.0
	12	man	-24.0	-2.0
	13	man	-16.0	-10.0
	14	glcnac	-8.0	-10.0
	15	glcnac	0.0	-0.0
	16	fuc	-16.0	6.0
	17	neuac	-49.0	-5.0
	18	fuc	-38.0	-27.0
	19	neuac	-50.0	-0.0
EDGE	18
	1	14:b1	15:4
	2	13:b1	14:4
	3	7:a1	13:3
	4	3:b1	7:2
	5	2:b1	3:4
	6	1:a2	2:3/6
	7	6:b1	7:4
	8	5:b1	6:4
	9	4:a2	5:3/6
	10	12:a1	13:6
	11	9:b1	12:2
	12	8:b1	9:4
	13	17:a2	8:3/6
	14	11:b1	12:6
	15	10:b1	11:4
	16	16:a1	15:6
	17	18:a1	3:3
	18	19:a2	10:3/6
///
ENTRY	4832_5_48	Glycan
NODE	21
	1	gal	-40.0	-20.0
	2	glcnac	-32.0	-20.0
	3	gal	-40.0	-16.0
	4	glcnac	-32.0	-16.0
	5	man	-24.0	-18.0
	6	gal	-40.0	-6.0
	7	glcnac	-32.0	-6.0
	8	gal	-40.0	-0.0
	9	glcnac	-32.0	2.0
	10	man	-24.0	-2.0
	11	man	-16.0	-10.0
	12	glcnac	-8.0	-10.0
	13	glcnac	0.0	-0.0
	14	glcnac	-48.0	8.0
	15	gal	-57.0	10.0
	16	neuac	-52.0	-14.0
	17	neuac	-51.0	-2.0
	18	fuc	-6.0	11.0
	19	fuc	-40.0	-9.0
	20	fuc	-40.0	-25.0
	21	fuc	-38.0	-2.0
EDGE	20
	1	12:b1	13:4
	2	11:b1	12:4
	3	5:a1	11:3
	4	2:b1	5:2
	5	1:b1	2:4
	6	16:a2	1:3/6
	7	20:a1	2:3
	8	4:b1	5:4
	9	3:b1	4:4
	10	17:a2	3:3/6
	11	19:a1	4:3
	12	10:a1	11:6
	13	7:b1	10:2
	14	6:b1	7:4
	15	14:b1	6:3
	16	15:b1	14:4
	17	21:a1	7:3
	18	9:b1	10:6
	19	8:b1	9:4
	20	18:a1	13:6
///
ENTRY	4945_1_2	Glycan
NODE	18
	1	gal	-40.0	-20.0
	2	glcnac	-32.0	-20.0
	3	gal	-40.0	-16.0
	4	glcnac	-32.0	-16.0
	5	man	-24.0	-18.0
	6	gal	-40.0	-6.0
	7	glcnac	-32.0	-6.0
	8	gal	-40.0	-0.0
	9	glcnac	-32.0	2.0
	10	man	-24.0	-2.0
	11	man	-16.0	-10.0
	12	glcnac	-8.0	-10.0
	13	glcnac	0.0	-0.0
	14	glcnac	-48.0	8.0
	15	gal	-57.0	10.0
	16	neuac	-52.0	-14.0
	17	neuac	-51.0	-2.0
	18	neuac	-49.0	3.0
EDGE	17
	1	12:b1	13:4
	2	11:b1	12:4
	3	5:a1	11:3
	4	2:b1	5:2
	5	1:b1	2:4
	6	16:a2	1:3/6
	7	4:b1	5:4
	8	3:b1	4:4
	9	17:a2	3:3/6
	10	10:a1	11:6
	11	7:b1	10:2
	12	6:b1	7:4
	13	18:a2	6:3/6
	14	9:b1	10:6
	15	8:b1	9:4
	16	14:b1	8:3
	17	15:b1	14:4
///
ENTRY	638_1	Glycan
NODE	3
	1	gal	-16.0	2.0
	2	fuc	-16.0	6.0
	3	glcnac	-8.0	4.0
EDGE	2
	1	1:b1	3:4
	2	2:a1	3:3
///
			   </textarea><BR>
			   Or load KCF from a file: <BR>
			<input name="targetFILE" type="file">

			   </TD>
			<TD>
            		   <font size=3><b>Control data&nbsp;<br></b></font>
			   <textarea name ="control" rows="15" cols="25">
ENTRY	1099_6	Glycan
NODE	4
	1	gal	-17.0	9.0
	2	glcnac	-8.0	9.0
	3	man	1.0	9.0
	4	neuac	-25.0	9.0
EDGE	3
	1	2:b1	3:2
	2	1:b1	2:4
	3	4:a2	1:3
///
ENTRY	1273_3	Glycan
NODE	5
	1	gal	-17.0	9.0
	2	glcnac	-8.0	9.0
	3	man	1.0	9.0
	4	fuc	-8.0	2.0
	5	neuac	-25.0	9.0
EDGE	4
	1	2:b1	3:2
	2	1:b1	2:4
	3	4:a1	2:3
	4	5:a2	1:3
///
ENTRY	1344_4_2	Glycan
NODE	5
	1	gal	-8.0	-2.0
	2	gal	-16.0	2.0
	3	glcnac	-8.0	2.0
	4	galnac	0.0	-0.0
	5	neuac	-16.0	-2.0
EDGE	4
	1	1:b1	4:3
	2	3:b1	4:6
	3	2:b1	3:4
	4	5:a2	1:3
///
ENTRY	1617_4_1	Glycan
NODE	5
	1	neuac	-16.0	-2.0
	2	gal	-8.0	-2.0
	3	neuac	-8.0	2.0
	4	galnac	0.0	-0.0
	5	neuac	-16.0	2.0
EDGE	4
	1	2:b1	4:3
	2	1:a2	2:3
	3	3:a2	4:6
	4	5:a2	3:8
///
ENTRY	1906_9_1520_4	Glycan
NODE	8
	1	glcnac	-32.0	-6.0
	2	glcnac	-32.0	-2.0
	3	man	-24.0	-4.0
	4	glcnac	-32.0	4.0
	5	man	-24.0	4.0
	6	man	-16.0	0.0
	7	glcnac	-8.0	0.0
	8	glcnac	0.0	0.0
EDGE	7
	1	1:b1	3:2
	2	2:b1	3:4
	3	3:a1	6:3
	4	4:b1	5:2
	5	5:a1	6:6
	6	6:b1	7:4
	7	7:b1	8:4
///
ENTRY	2080_9_	Glycan
NODE	9
	1	glcnac	-32.0	-12.0
	2	glcnac	-32.0	-8.0
	3	man	-24.0	-10.0
	4	glcnac	-32.0	-2.0
	5	man	-24.0	-2.0
	6	man	-16.0	-6.0
	7	glcnac	-8.0	-6.0
	8	fuc	-8.0	6.0
	9	glcnac	0.0	0.0
EDGE	8
	1	1:b1	3:2
	2	2:b1	3:4
	3	3:a1	6:3
	4	4:b1	5:2
	5	5:a1	6:6
	6	6:b1	7:4
	7	7:b1	9:4
	8	8:a1	9:6
///
ENTRY	2622_1_2095_92	Glycan
NODE	12
	1	gal	-40.0	-12.0
	2	fuc	-40.0	-8.0
	3	glcnac	-32.0	-10.0
	4	man	-24.0	-10.0
	5	gal	-48.0	-2.0
	6	gal	-40.0	-2.0
	7	glcnac	-32.0	-2.0
	8	man	-24.0	-2.0
	9	man	-16.0	-6.0
	10	glcnac	-8.0	-6.0
	11	fuc	-8.0	6.0
	12	glcnac	0.0	0.0
EDGE	11
	1	1:b1	3:4
	2	2:a1	3:3
	3	3:b1	4:2
	4	4:a1	9:3
	5	5:a1	6:3
	6	6:b1	7:4
	7	7:b1	8:2
	8	8:a1	9:6
	9	9:b1	10:4
	10	10:b1	12:4
	11	11:a1	12:6
///
ENTRY	2779_3_2225_03	Glycan
NODE	12
	1	neuac	-48.0	-10.0
	2	gal	-40.0	-10.0
	3	glcnac	-32.0	-10.0
	4	man	-24.0	-10.0
	5	gal	-40.0	-4.0
	6	fuc	-40.0	0.0
	7	glcnac	-32.0	-2.0
	8	man	-24.0	-2.0
	9	man	-16.0	-6.0
	10	glcnac	-8.0	-6.0
	11	fuc	-8.0	6.0
	12	glcnac	0.0	0.0
EDGE	11
	1	1:a2	2:6
	2	2:b1	3:4
	3	3:b1	4:2
	4	4:a1	9:3
	5	5:b1	7:4
	6	6:a1	7:3
	7	7:b1	8:2
	8	8:a1	9:6
	9	9:b1	10:4
	10	10:b1	12:4
	11	11:a1	12:6
///
ENTRY	2837_3_2283_11	Glycan
NODE	13
	1	glcnac	-24.0	-18.0
	2	gal	-40.0	-12.0
	3	fuc	-40.0	-8.0
	4	glcnac	-32.0	-10.0
	5	man	-24.0	-10.0
	6	gal	-40.0	-4.0
	7	fuc	-40.0	0.0
	8	glcnac	-32.0	-2.0
	9	man	-24.0	-2.0
	10	man	-16.0	-10.0
	11	glcnac	-8.0	-10.0
	12	fuc	-8.0	10.0
	13	glcnac	0.0	0.0
EDGE	12
	1	1:b1	10:4
	2	2:b1	4:4
	3	3:a1	4:3
	4	4:b1	5:2
	5	5:a1	10:3
	6	6:b1	8:4
	7	7:a1	8:3
	8	8:b1	9:2
	9	9:a1	10:6
	10	10:b1	11:4
	11	11:b1	13:4
	12	12:a1	13:6
///
ENTRY	3024_3_2428_23	Glycan
NODE	13
	1	glcnac	-24.0	-14.0
	2	gal	-40.0	-10.0
	3	fuc	-40.0	-6.0
	4	glcnac	-32.0	-8.0
	5	man	-24.0	-8.0
	6	neuac	-48.0	-2.0
	7	gal	-40.0	-2.0
	8	glcnac	-32.0	-2.0
	9	man	-24.0	-2.0
	10	man	-16.0	-8.0
	11	glcnac	-8.0	-8.0
	12	fuc	-8.0	8.0
	13	glcnac	0.0	0.0
EDGE	12
	1	1:b1	10:4
	2	2:b1	4:4
	3	3:a1	4:3
	4	4:b1	5:2
	5	5:a1	10:3
	6	6:a2	7:3/6
	7	7:b1	8:4
	8	8:b1	9:2
	9	9:a1	10:6
	10	10:b1	11:4
	11	11:b1	13:4
	12	12:a1	13:6
///
ENTRY	3026_3	Glycan
NODE	12
	1	gal	-40.0	-6.0
	2	glcnac	-32.0	-6.0
	3	man	-24.0	-6.0
	4	gal	-40.0	-2.0
	5	glcnac	-32.0	-2.0
	6	man	-24.0	-2.0
	7	man	-16.0	-4.0
	8	glcnac	-8.0	-4.0
	9	fuc	-8.0	4.0
	10	glcnac	0.0	-0.0
	11	neugc	-50.0	-2.0
	12	neugc	-50.0	-6.0
EDGE	11
	1	8:b1	10:4
	2	7:b1	8:4
	3	3:a1	7:3
	4	2:b1	3:2
	5	1:b1	2:4
	6	12:a2	1:3
	7	6:a1	7:6
	8	5:b1	6:2
	9	4:b1	5:4
	10	11:a2	4:3
	11	9:a1	10:6
///
ENTRY	3215_4_2591_4	Glycan
NODE	15
	1	gal	-40.0	-30.0
	2	fuc	-40.0	-26.0
	3	glcnac	-32.0	-28.0
	4	gal	-40.0	-18.0
	5	fuc	-40.0	-14.0
	6	glcnac	-32.0	-16.0
	7	man	-24.0	-22.0
	8	gal	-40.0	-4.0
	9	fuc	-40.0	0.0
	10	glcnac	-32.0	-2.0
	11	man	-24.0	-2.0
	12	man	-16.0	-12.0
	13	glcnac	-8.0	-12.0
	14	fuc	-8.0	12.0
	15	glcnac	0.0	0.0
EDGE	14
	1	1:b1	3:4
	2	2:a1	3:3
	3	3:b1	7:2
	4	4:b1	6:4
	5	5:a1	6:3
	6	6:b1	7:4
	7	7:a1	12:3
	8	8:b1	10:4
	9	9:a1	10:3
	10	10:b1	11:2
	11	11:a1	12:6
	12	12:b1	13:4
	13	13:b1	15:4
	14	14:a1	15:6
///
ENTRY	3269_4_2631_42	Glycan
NODE	14
	1	glcnac	-24.0	-18.0
	2	neuac	-48.0	-10.0
	3	gal	-40.0	-10.0
	4	glcnac	-32.0	-10.0
	5	man	-24.0	-10.0
	6	glcnac	-32.0	-6.0
	7	gal	-40.0	0.0
	8	fuc	-40.0	4.0
	9	glcnac	-32.0	2.0
	10	man	-24.0	-2.0
	11	man	-16.0	-10.0
	12	glcnac	-8.0	-10.0
	13	fuc	-8.0	10.0
	14	glcnac	0.0	0.0
EDGE	13
	1	1:b1	11:4
	2	2:a2	3:3/6
	3	3:b1	4:4
	4	4:b1	5:2
	5	5:a1	11:3
	6	6:b1	10:2
	7	7:b1	9:4
	8	8:a1	9:3
	9	9:b1	10:6
	10	10:a1	11:6
	11	11:b1	12:4
	12	12:b1	14:4
	13	13:a1	14:6
///
ENTRY	3402_4_2736_51	Glycan
NODE	15
	1	gal	-40.0	-20.0
	2	fuc	-40.0	-16.0
	3	glcnac	-32.0	-18.0
	4	man	-24.0	-18.0
	5	neuac	-48.0	-6.0
	6	gal	-40.0	-6.0
	7	glcnac	-32.0	-6.0
	8	gal	-40.0	0.0
	9	fuc	-40.0	4.0
	10	glcnac	-32.0	2.0
	11	man	-24.0	-2.0
	12	man	-16.0	-10.0
	13	glcnac	-8.0	-10.0
	14	fuc	-8.0	10.0
	15	glcnac	0.0	0.0
EDGE	14
	1	1:b1	3:4
	2	2:a1	3:3
	3	3:b1	4:2
	4	4:a1	12:3
	5	5:a2	6:3/6
	6	6:b1	7:4
	7	7:b1	11:2
	8	8:b1	10:4
	9	9:a1	10:3
	10	10:b1	11:6
	11	11:a1	12:6
	12	12:b1	13:4
	13	13:b1	15:4
	14	14:a1	15:6
///
ENTRY	3460_4_2794_59	Glycan
NODE	16
	1	glcnac	-24.0	-26.0
	2	gal	-40.0	-22.0
	3	fuc	-40.0	-18.0
	4	glcnac	-32.0	-20.0
	5	gal	-40.0	-10.0
	6	fuc	-40.0	-6.0
	7	glcnac	-32.0	-8.0
	8	man	-24.0	-14.0
	9	gal	-40.0	-4.0
	10	fuc	-40.0	0.0
	11	glcnac	-32.0	-2.0
	12	man	-24.0	-2.0
	13	man	-16.0	-14.0
	14	glcnac	-8.0	-14.0
	15	fuc	-8.0	14.0
	16	glcnac	0.0	0.0
EDGE	15
	1	1:b1	13:4
	2	2:b1	4:4
	3	3:a1	4:3
	4	4:b1	8:2
	5	5:b1	7:4
	6	6:a1	7:3
	7	7:b1	8:4
	8	8:a1	13:3
	9	9:b1	11:4
	10	10:a1	11:3
	11	11:b1	12:2
	12	12:a1	13:6
	13	13:b1	14:4
	14	14:b1	16:4
	15	15:a1	16:6
///
ENTRY	3515_5_2834_62	Glycan
NODE	15
	1	glcnac	-24.0	-22.0
	2	glcnac	-32.0	-14.0
	3	glcnac	-32.0	-10.0
	4	man	-24.0	-12.0
	5	neuac	-48.0	-6.0
	6	gal	-40.0	-6.0
	7	glcnac	-32.0	-6.0
	8	gal	-40.0	0.0
	9	fuc	-40.0	4.0
	10	glcnac	-32.0	2.0
	11	man	-24.0	-2.0
	12	man	-16.0	-12.0
	13	glcnac	-8.0	-12.0
	14	fuc	-8.0	12.0
	15	glcnac	0.0	0.0
EDGE	14
	1	1:b1	12:4
	2	2:b1	4:2
	3	3:b1	4:4
	4	4:a1	12:3
	5	5:a2	6:3/6
	6	6:b1	7:4
	7	7:b1	11:2
	8	8:b1	10:4
	9	9:a1	10:3
	10	10:b1	11:6
	11	11:a1	12:6
	12	12:b1	13:4
	13	13:b1	15:4
	14	14:a1	15:6
///
ENTRY	3589_4_2881_63	Glycan
NODE	15
	1	neuac	-48.0	-14.0
	2	gal	-40.0	-14.0
	3	glcnac	-32.0	-14.0
	4	man	-24.0	-14.0
	5	neuac	-48.0	-6.0
	6	gal	-40.0	-6.0
	7	glcnac	-32.0	-6.0
	8	gal	-40.0	0.0
	9	fuc	-40.0	4.0
	10	glcnac	-32.0	2.0
	11	man	-24.0	-2.0
	12	man	-16.0	-8.0
	13	glcnac	-8.0	-8.0
	14	fuc	-8.0	8.0
	15	glcnac	0.0	0.0
EDGE	14
	1	1:a2	2:3/6
	2	2:b1	3:4
	3	3:b1	4:2
	4	4:a1	12:3
	5	5:a2	6:3/6
	6	6:b1	7:4
	7	7:b1	11:2
	8	8:b1	10:4
	9	9:a1	10:3
	10	10:b1	11:6
	11	11:a1	12:6
	12	12:b1	13:4
	13	13:b1	15:2
	14	14:a1	15:6
///
ENTRY	3647_5_2939_71	Glycan
NODE	16
	1	glcnac	-24.0	-22.0
	2	neuac	-48.0	-16.0
	3	gal	-40.0	-16.0
	4	glcnac	-32.0	-16.0
	5	gal	-40.0	-10.0
	6	fuc	-40.0	-6.0
	7	glcnac	-32.0	-8.0
	8	man	-24.0	-12.0
	9	gal	-40.0	-4.0
	10	fuc	-40.0	0.0
	11	glcnac	-32.0	-2.0
	12	man	-24.0	-2.0
	13	man	-16.0	-12.0
	14	glcnac	-8.0	-12.0
	15	fuc	-8.0	12.0
	16	glcnac	0.0	0.0
EDGE	15
	1	1:b1	13:4
	2	2:a2	3:3/6
	3	3:b1	4:4
	4	4:b1	8:2
	5	5:b1	7:4
	6	6:a1	7:3
	7	7:b1	8:4
	8	8:a1	13:3
	9	9:b1	11:4
	10	10:a1	11:3
	11	11:b1	12:2
	12	12:a1	13:6
	13	13:b1	14:4
	14	14:b1	16:4
	15	15:a1	16:6
///
ENTRY	3705_5_2997_79	Glycan
NODE	17
	1	glcnac	-24.0	-30.0
	2	gal	-40.0	-24.0
	3	fuc	-40.0	-20.0
	4	glcnac	-32.0	-22.0
	5	gal	-40.0	-12.0
	6	fuc	-40.0	-8.0
	7	glcnac	-32.0	-10.0
	8	man	-24.0	-16.0
	9	glcnac	-32.0	-6.0
	10	gal	-40.0	0.0
	11	fuc	-40.0	4.0
	12	glcnac	-32.0	2.0
	13	man	-24.0	-2.0
	14	man	-16.0	-16.0
	15	glcnac	-8.0	-16.0
	16	fuc	-8.0	16.0
	17	glcnac	0.0	0.0
EDGE	16
	1	1:b1	14:4
	2	2:b1	4:4
	3	3:a1	4:3
	4	4:b1	8:2
	5	5:b1	7:4
	6	6:a1	7:3
	7	7:b1	8:4
	8	8:a1	14:3
	9	9:b1	13:2
	10	10:b1	12:4
	11	11:a1	12:3
	12	12:b1	13:6
	13	13:a1	14:6
	14	14:b1	15:4
	15	15:b1	17:4
	16	16:a1	17:6
///
ENTRY	3839_6	Glycan
NODE	19
	1	glcnac	-24.0	-30.0
	2	gal	-40.0	-24.0
	3	fuc	-40.0	-20.0
	4	glcnac	-32.0	-22.0
	5	gal	-40.0	-12.0
	6	fuc	-40.0	-8.0
	7	glcnac	-32.0	-10.0
	8	man	-24.0	-16.0
	9	glcnac	-32.0	-6.0
	10	gal	-40.0	-0.0
	11	fuc	-40.0	4.0
	12	glcnac	-32.0	2.0
	13	man	-24.0	-2.0
	14	man	-16.0	-16.0
	15	glcnac	-8.0	-16.0
	16	fuc	-8.0	16.0
	17	glcnac	0.0	-0.0
	18	fuc	-40.0	-2.0
	19	gal	-40.0	-6.0
EDGE	18
	1	15:b1	17:4
	2	14:b1	15:4
	3	1:b1	14:4
	4	8:a1	14:3
	5	4:b1	8:2
	6	2:b1	4:4
	7	3:a1	4:3
	8	7:b1	8:4
	9	5:b1	7:4
	10	6:a1	7:3
	11	13:a1	14:6
	12	9:b1	13:2
	13	12:b1	13:6
	14	10:b1	12:4
	15	11:a1	12:3
	16	16:a1	17:6
	17	18:a1	9:3
	18	19:b1	9:4
///
ENTRY	4025_7_3247_99	Glycan
NODE	18
	1	gal	-40.0	-34.0
	2	fuc	-40.0	-30.0
	3	glcnac	-32.0	-32.0
	4	gal	-40.0	-22.0
	5	fuc	-40.0	-18.0
	6	glcnac	-32.0	-20.0
	7	man	-24.0	-26.0
	8	neuac	-48.0	-6.0
	9	gal	-40.0	-6.0
	10	glcnac	-32.0	-6.0
	11	gal	-40.0	0.0
	12	fuc	-40.0	4.0
	13	glcnac	-32.0	2.0
	14	man	-24.0	-2.0
	15	man	-16.0	-14.0
	16	glcnac	-8.0	-14.0
	17	fuc	-8.0	14.0
	18	glcnac	0.0	0.0
EDGE	17
	1	1:b1	3:4
	2	2:a1	3:3
	3	3:b1	7:2
	4	4:b1	6:4
	5	5:a1	6:3
	6	6:b1	7:4
	7	7:a1	15:3
	8	8:a2	9:3/6
	9	9:b1	10:4
	10	10:b1	14:2
	11	11:b1	13:4
	12	12:a1	13:3
	13	13:b1	14:6
	14	14:a1	15:6
	15	15:b1	16:4
	16	16:b1	18:4
	17	17:a1	18:6
///
ENTRY	4213_6_3393_11	Glycan
NODE	18
	1	gal	-40.0	-28.0
	2	fuc	-40.0	-24.0
	3	glcnac	-32.0	-26.0
	4	neuac	-48.0	-18.0
	5	gal	-40.0	-18.0
	6	glcnac	-32.0	-18.0
	7	man	-24.0	-22.0
	8	neuac	-48.0	-6.0
	9	gal	-40.0	-6.0
	10	glcnac	-32.0	-6.0
	11	gal	-40.0	0.0
	12	fuc	-40.0	4.0
	13	glcnac	-32.0	2.0
	14	man	-24.0	-2.0
	15	man	-16.0	-12.0
	16	glcnac	-8.0	-12.0
	17	fuc	-8.0	12.0
	18	glcnac	0.0	0.0
EDGE	17
	1	1:b1	3:4
	2	2:a1	3:3
	3	3:b1	7:2
	4	4:a2	5:3/6
	5	5:b1	6:4
	6	6:b1	7:4
	7	7:a1	15:3
	8	8:a2	9:3/6
	9	9:b1	10:4
	10	10:b1	14:2
	11	11:b1	13:4
	12	12:a1	13:3
	13	13:b1	14:6
	14	14:a1	15:6
	15	15:b1	16:4
	16	16:b1	18:4
	17	17:a1	18:6
///
ENTRY	4401_5_3538_22	Glycan
NODE	18
	1	neuac	-48.0	-20.0
	2	gal	-40.0	-20.0
	3	glcnac	-32.0	-20.0
	4	neuac	-48.0	-16.0
	5	gal	-40.0	-16.0
	6	glcnac	-32.0	-16.0
	7	man	-24.0	-18.0
	8	neuac	-48.0	-6.0
	9	gal	-40.0	-6.0
	10	glcnac	-32.0	-6.0
	11	gal	-40.0	0.0
	12	fuc	-40.0	4.0
	13	glcnac	-32.0	2.0
	14	man	-24.0	-2.0
	15	man	-16.0	-10.0
	16	glcnac	-8.0	-10.0
	17	fuc	-8.0	10.0
	18	glcnac	0.0	0.0
EDGE	17
	1	1:a2	2:3/6
	2	2:b1	3:4
	3	3:b1	7:2
	4	4:a2	5:3/6
	5	5:b1	6:4
	6	6:b1	7:4
	7	7:a1	15:3
	8	8:a2	9:3/6
	9	9:b1	10:4
	10	10:b1	14:2
	11	11:b1	13:4
	12	12:a1	13:3
	13	13:b1	14:6
	14	14:a1	15:6
	15	15:b1	16:4
	16	16:b1	18:4
	17	17:a1	18:6
///
ENTRY	912_2	Glycan
NODE	4
	1	gal	-17.0	9.0
	2	glcnac	-8.0	9.0
	3	man	1.0	9.0
	4	fuc	-8.0	2.0
EDGE	3
	1	2:b1	3:2
	2	1:b1	2:4
	3	4:a1	2:3
///
ENTRY	W1099_6_2	Glycan
NODE	4
	1	gal	-8.0	-2.0
	2	galnac	0.0	-0.0
	3	gal	-8.0	4.0
	4	neuac	-16.0	-3.0
EDGE	3
	1	1:b1	2:3
	2	3:b1	2:4
	3	4:a2	1:6
///
ENTRY	W1999_0	Glycan
NODE	9
	1	man	-24.0	-4.0
	2	man	-32.0	2.0
	3	man	-32.0	6.0
	4	man	-24.0	4.0
	5	man	-16.0	-0.0
	6	glcnac	-8.0	-0.0
	7	glcnac	0.0	-0.0
	8	glcnac	-32.0	-4.0
	9	fuc	0.0	11.0
EDGE	8
	1	6:b1	7:4
	2	5:b1	6:4
	3	1:a1	5:3
	4	4:a1	5:6
	5	2:a1	4:3
	6	3:a1	4:6
	7	8:b1	1:2
	8	7:a1	9:6
///
ENTRY	W2203_1	Glycan
NODE	10
	1	man	-24.0	-4.0
	2	man	-32.0	2.0
	3	man	-32.0	6.0
	4	man	-24.0	4.0
	5	man	-16.0	-0.0
	6	glcnac	-8.0	-0.0
	7	glcnac	0.0	-0.0
	8	glcnac	-32.0	-4.0
	9	fuc	0.0	11.0
	10	gal	-40.0	-4.0
EDGE	9
	1	6:b1	7:4
	2	5:b1	6:4
	3	1:a1	5:3
	4	4:a1	5:6
	5	2:a1	4:3
	6	3:a1	4:6
	7	8:b1	1:2
	8	7:a1	9:6
	9	10:b1	8:4
///
ENTRY	W2326_2	Glycan
NODE	10
	1	glcnac	-24.0	-14.0
	2	glcnac	-32.0	-10.0
	3	glcnac	-32.0	-6.0
	4	man	-24.0	-8.0
	5	glcnac	-32.0	-2.0
	6	man	-24.0	-2.0
	7	man	-16.0	-8.0
	8	glcnac	-8.0	-8.0
	9	fuc	-8.0	8.0
	10	glcnac	0.0	-0.0
EDGE	9
	1	8:b1	10:4
	2	7:b1	8:4
	3	1:b1	7:4
	4	4:a1	7:3
	5	2:b1	4:2
	6	3:b1	4:4
	7	6:a1	7:6
	8	5:b1	6:2
	9	9:a1	10:6
///
ENTRY	W2448_3	Glycan
NODE	11
	1	man	-24.0	-4.0
	2	man	-32.0	2.0
	3	man	-32.0	6.0
	4	man	-24.0	4.0
	5	man	-16.0	-0.0
	6	glcnac	-8.0	-0.0
	7	glcnac	0.0	-0.0
	8	glcnac	-32.0	-4.0
	9	fuc	0.0	11.0
	10	gal	-40.0	-4.0
	11	glcnac	-22.0	-8.0
EDGE	10
	1	6:b1	7:4
	2	5:b1	6:4
	3	1:a1	5:3
	4	8:b1	1:2
	5	10:b1	8:4
	6	4:a1	5:6
	7	2:a1	4:3
	8	3:a1	4:6
	9	9:6	7:a1
	10	11:b1	5:4
///
ENTRY	W2564_3	Glycan
NODE	11
	1	man	-24.0	-4.0
	2	man	-32.0	2.0
	3	man	-32.0	6.0
	4	man	-24.0	4.0
	5	man	-16.0	-0.0
	6	glcnac	-8.0	-0.0
	7	glcnac	0.0	-0.0
	8	glcnac	-32.0	-4.0
	9	fuc	0.0	11.0
	10	gal	-40.0	-4.0
	11	neuac	-48.0	-4.0
EDGE	10
	1	6:b1	7:4
	2	5:b1	6:4
	3	1:a1	5:3
	4	8:b1	1:2
	5	10:b1	8:4
	6	4:a1	5:6
	7	2:a1	4:3
	8	3:a1	4:6
	9	9:6	7:a1
	10	11:a2	10:3/6
///
ENTRY	W2908_5	Glycan
NODE	13
	1	glcnac	-24.0	-18.0
	2	gal	-40.0	-16.0
	3	fuc	-40.0	-12.0
	4	glcnac	-32.0	-14.0
	5	glcnac	-32.0	-6.0
	6	man	-24.0	-10.0
	7	glcnac	-32.0	-2.0
	8	man	-24.0	-2.0
	9	man	-16.0	-10.0
	10	glcnac	-8.0	-10.0
	11	fuc	-8.0	10.0
	12	glcnac	0.0	-0.0
	13	gal	-41.0	-5.0
EDGE	12
	1	10:b1	12:4
	2	9:b1	10:4
	3	1:b1	9:4
	4	6:a1	9:3
	5	4:b1	6:2
	6	2:b1	4:4
	7	3:a1	4:3
	8	5:b1	6:4
	9	8:a1	9:6
	10	7:b1	8:2
	11	11:a1	12:6
	12	13:b1	5:4
///
ENTRY	W3024_5	Glycan
NODE	13
	1	glcnac	-24.0	-14.0
	2	gal	-42.0	-0.0
	3	fuc	-40.0	8.0
	4	glcnac	-33.0	-1.0
	5	man	-24.0	-1.0
	6	neuac	-48.0	-8.0
	7	gal	-40.0	-8.0
	8	glcnac	-33.0	-8.0
	9	man	-24.0	-8.0
	10	man	-16.0	-8.0
	11	glcnac	-8.0	-8.0
	12	fuc	-8.0	8.0
	13	glcnac	0.0	-0.0
EDGE	12
	1	11:b1	13:4
	2	10:b1	11:4
	3	1:b1	10:4
	4	5:a1	10:3
	5	4:b1	5:2
	6	2:b1	4:4
	7	3:a1	4:3
	8	9:a1	10:6
	9	8:b1	9:2
	10	7:b1	8:4
	11	6:a2	7:3/6
	12	12:a1	13:6
///
ENTRY	W3041_6	Glycan
NODE	14
	1	gal	-40.0	-30.0
	2	fuc	-40.0	-26.0
	3	glcnac	-32.0	-28.0
	4	gal	-40.0	-18.0
	5	fuc	-40.0	-14.0
	6	glcnac	-32.0	-16.0
	7	man	-24.0	-22.0
	8	gal	-40.0	-4.0
	9	glcnac	-32.0	-2.0
	10	man	-24.0	-2.0
	11	man	-16.0	-12.0
	12	glcnac	-8.0	-12.0
	13	fuc	-8.0	12.0
	14	glcnac	0.0	-0.0
EDGE	13
	1	12:b1	14:4
	2	11:b1	12:4
	3	7:a1	11:3
	4	3:b1	7:2
	5	1:b1	3:4
	6	2:a1	3:3
	7	6:b1	7:4
	8	4:b1	6:4
	9	5:a1	6:3
	10	10:a1	11:6
	11	9:b1	10:2
	12	8:b1	9:4
	13	13:a1	14:6
///
ENTRY	W3269_6	Glycan
NODE	14
	1	glcnac	-24.0	-18.0
	2	neuac	-48.0	-10.0
	3	gal	-40.0	-10.0
	4	glcnac	-32.0	-10.0
	5	man	-24.0	-10.0
	6	glcnac	-31.0	5.0
	7	gal	-40.0	-0.0
	8	fuc	-40.0	4.0
	9	glcnac	-32.0	-1.0
	10	man	-24.0	-2.0
	11	man	-16.0	-10.0
	12	glcnac	-8.0	-10.0
	13	fuc	-8.0	10.0
	14	glcnac	0.0	-0.0
EDGE	13
	1	12:b1	14:4
	2	11:b1	12:4
	3	1:b1	11:4
	4	5:a1	11:3
	5	4:b1	5:2
	6	3:b1	4:4
	7	2:a2	3:3/6
	8	10:a1	11:6
	9	6:b1	10:2
	10	7:b1	9:4
	11	8:a1	9:3
	12	13:a1	14:6
	13	9:b1	5:4
///
ENTRY	W3327_7	Glycan
NODE	15
	1	glcnac	-24.0	-26.0
	2	gal	-40.0	-22.0
	3	fuc	-40.0	-18.0
	4	glcnac	-32.0	-20.0
	5	gal	-40.0	-10.0
	6	fuc	-40.0	-6.0
	7	glcnac	-32.0	-8.0
	8	man	-24.0	-14.0
	9	glcnac	-32.0	-4.0
	10	glcnac	-32.0	-0.0
	11	man	-24.0	-2.0
	12	man	-16.0	-14.0
	13	glcnac	-8.0	-14.0
	14	fuc	-8.0	14.0
	15	glcnac	0.0	-0.0
EDGE	14
	1	13:b1	15:4
	2	12:b1	13:4
	3	1:b1	12:4
	4	8:a1	12:3
	5	4:b1	8:2
	6	2:b1	4:4
	7	3:a1	4:3
	8	7:b1	8:4
	9	5:b1	7:4
	10	6:a1	7:3
	11	11:a1	12:6
	12	9:b1	11:2
	13	10:b1	11:6
	14	14:a1	15:6
///
ENTRY	W3402_7	Glycan
NODE	15
	1	neuac	-48.0	-16.0
	2	gal	-40.0	-16.0
	3	glcnac	-32.0	-16.0
	4	gal	-40.0	-10.0
	5	glcnac	-32.0	-8.0
	6	man	-24.0	-12.0
	7	gal	-40.0	-4.0
	8	glcnac	-32.0	-2.0
	9	man	-24.0	-2.0
	10	man	-16.0	-12.0
	11	glcnac	-8.0	-12.0
	12	fuc	-8.0	12.0
	13	glcnac	0.0	-0.0
	14	fuc	-34.0	8.0
	15	fuc	-34.0	-12.0
EDGE	14
	1	11:b1	13:4
	2	10:b1	11:4
	3	6:a1	10:3
	4	3:b1	6:2
	5	2:b1	3:4
	6	1:a2	2:3/6
	7	5:b1	6:4
	8	4:b1	5:4
	9	15:a1	5:3
	10	9:a1	10:6
	11	8:b1	9:2
	12	7:b1	8:4
	13	14:a1	8:3
	14	12:a1	13:6
///
ENTRY	W3514_9	Glycan
NODE	15
	1	glcnac	-24.0	-22.0
	2	glcnac	-34.0	1.0
	3	glcnac	-33.0	7.0
	4	man	-24.0	-0.0
	5	neuac	-50.0	-12.0
	6	gal	-43.0	-12.0
	7	glcnac	-35.0	-12.0
	8	gal	-43.0	-6.0
	9	fuc	-42.0	-1.0
	10	glcnac	-36.0	-6.0
	11	man	-26.0	-12.0
	12	man	-16.0	-12.0
	13	glcnac	-8.0	-12.0
	14	fuc	-8.0	12.0
	15	glcnac	0.0	-0.0
EDGE	14
	1	13:b1	15:4
	2	12:b1	13:4
	3	1:b1	12:4
	4	4:a1	12:3
	5	2:b1	4:2
	6	3:b1	4:6
	7	11:a1	12:6
	8	7:b1	11:2
	9	6:b1	7:4
	10	5:a2	6:3/6
	11	10:b1	11:4
	12	8:b1	10:4
	13	9:a1	10:3
	14	14:a1	15:6
///
ENTRY	W3531_7	Glycan
NODE	16
	1	glcnac	-24.0	-26.0
	2	gal	-40.0	-22.0
	3	fuc	-40.0	-18.0
	4	glcnac	-32.0	-20.0
	5	gal	-40.0	-10.0
	6	fuc	-40.0	-6.0
	7	glcnac	-32.0	-8.0
	8	man	-24.0	-14.0
	9	gal	-40.0	-4.0
	10	glcnac	-32.0	-4.0
	11	glcnac	-32.0	-0.0
	12	man	-24.0	-2.0
	13	man	-16.0	-14.0
	14	glcnac	-8.0	-14.0
	15	fuc	-8.0	14.0
	16	glcnac	0.0	-0.0
EDGE	15
	1	14:b1	16:4
	2	13:b1	14:4
	3	1:b1	13:4
	4	8:a1	13:3
	5	4:b1	8:2
	6	2:b1	4:4
	7	3:a1	4:3
	8	7:b1	8:4
	9	5:b1	7:4
	10	6:a1	7:3
	11	12:a1	13:6
	12	10:b1	12:2
	13	9:b1	10:4
	14	11:b1	12:6
	15	15:a1	16:6
///
ENTRY	W3589_8	Glycan
NODE	15
	1	neuac	-48.0	-14.0
	2	gal	-40.0	-14.0
	3	glcnac	-32.0	-14.0
	4	man	-24.0	-14.0
	5	neuac	-48.0	-6.0
	6	gal	-40.0	-6.0
	7	glcnac	-32.0	-6.0
	8	gal	-40.0	-0.0
	9	fuc	-40.0	4.0
	10	glcnac	-32.0	2.0
	11	man	-24.0	-2.0
	12	man	-16.0	-8.0
	13	glcnac	-8.0	-8.0
	14	fuc	-8.0	8.0
	15	glcnac	0.0	-0.0
EDGE	14
	1	13:b1	15:2
	2	12:b1	13:4
	3	4:a1	12:3
	4	3:b1	4:2
	5	2:b1	3:4
	6	1:a2	2:3/6
	7	11:a1	12:6
	8	6:b1	7:4
	9	5:a2	6:3/6
	10	10:b1	11:2
	11	8:b1	10:4
	12	9:a1	10:3
	13	14:a1	15:6
	14	7:b1	4:4
///
ENTRY	W3705_8	Glycan
NODE	16
	1	glcnac	-24.0	-30.0
	2	gal	-40.0	-24.0
	3	fuc	-40.0	-20.0
	4	glcnac	-32.0	-22.0
	5	gal	-40.0	-12.0
	6	fuc	-40.0	-8.0
	7	glcnac	-32.0	-10.0
	8	man	-24.0	-16.0
	9	gal	-40.0	-0.0
	10	fuc	-40.0	4.0
	11	glcnac	-32.0	2.0
	12	man	-24.0	-2.0
	13	man	-16.0	-16.0
	14	glcnac	-8.0	-16.0
	15	fuc	-8.0	16.0
	16	glcnac	0.0	-0.0
EDGE	15
	1	14:b1	16:4
	2	13:b1	14:4
	3	1:b1	13:4
	4	8:a1	13:3
	5	4:b1	8:2
	6	2:b1	4:4
	7	3:a1	4:3
	8	7:b1	8:4
	9	5:b1	7:4
	10	6:a1	7:3
	11	12:a1	13:6
	12	9:b1	11:4
	13	10:a1	11:3
	14	15:a1	16:6
	15	11:b1	12:2
///
ENTRY	W3834_9	Glycan
NODE	16
	1	glcnac	-24.0	-22.0
	2	neuac	-48.0	-16.0
	3	gal	-40.0	-16.0
	4	glcnac	-32.0	-16.0
	5	gal	-40.0	-10.0
	6	glcnac	-32.0	-8.0
	7	man	-24.0	-12.0
	8	glcnac	-32.0	-2.0
	9	man	-24.0	-2.0
	10	man	-16.0	-12.0
	11	glcnac	-8.0	-12.0
	12	fuc	-8.0	12.0
	13	glcnac	0.0	-0.0
	14	neuac	-48.0	-10.0
	15	gal	-41.0	-0.0
	16	fuc	-34.0	8.0
EDGE	15
	1	11:b1	13:4
	2	10:b1	11:4
	3	1:b1	10:4
	4	7:a1	10:3
	5	4:b1	7:2
	6	3:b1	4:4
	7	2:a2	3:3/6
	8	6:b1	7:4
	9	5:b1	6:4
	10	14:a2	5:3/6
	11	9:a1	10:6
	12	8:b1	9:2
	13	15:b1	8:4
	14	12:a1	13:6
	15	16:a1	8:3
///
ENTRY	W3892_8	Glycan
NODE	17
	1	glcnac	-24.0	-22.0
	2	neuac	-48.0	-16.0
	3	gal	-40.0	-16.0
	4	glcnac	-32.0	-16.0
	5	gal	-40.0	-10.0
	6	glcnac	-32.0	-8.0
	7	man	-24.0	-12.0
	8	glcnac	-32.0	-2.0
	9	man	-24.0	-2.0
	10	man	-16.0	-12.0
	11	glcnac	-8.0	-12.0
	12	fuc	-8.0	12.0
	13	glcnac	0.0	-0.0
	14	glcnac	-33.0	6.0
	15	gal	-40.0	-2.0
	16	fuc	-41.0	6.0
	17	fuc	-42.0	-5.0
EDGE	16
	1	11:b1	13:4
	2	10:b1	11:4
	3	1:b1	10:4
	4	7:a1	10:3
	5	4:b1	7:2
	6	3:b1	4:4
	7	2:a2	3:3/6
	8	6:b1	7:4
	9	5:b1	6:4
	10	17:a1	6:3
	11	9:a1	10:6
	12	8:b1	9:2
	13	15:b1	8:4
	14	16:a1	8:3
	15	14:b1	9:6
	16	12:a1	13:6
///
ENTRY	W4025_9	Glycan
NODE	18
	1	glcnac	-24.0	-22.0
	2	neuac	-48.0	-16.0
	3	gal	-40.0	-16.0
	4	glcnac	-32.0	-16.0
	5	gal	-40.0	-10.0
	6	glcnac	-32.0	-8.0
	7	man	-24.0	-12.0
	8	glcnac	-32.0	-2.0
	9	man	-24.0	-2.0
	10	man	-16.0	-12.0
	11	glcnac	-8.0	-12.0
	12	fuc	-8.0	12.0
	13	glcnac	0.0	-0.0
	14	glcnac	-33.0	6.0
	15	gal	-40.0	-2.0
	16	fuc	-41.0	6.0
	17	fuc	-42.0	-5.0
	18	gal	-41.0	9.0
EDGE	17
	1	11:b1	13:4
	2	10:b1	11:4
	3	1:b1	10:4
	4	7:a1	10:3
	5	4:b1	7:2
	6	3:b1	4:4
	7	2:a2	3:3/6
	8	6:b1	7:4
	9	5:b1	6:4
	10	17:a1	6:3
	11	9:a1	10:6
	12	8:b1	9:2
	13	15:b1	8:4
	14	16:a1	8:3
	15	14:b1	9:6
	16	12:a1	13:6
	17	18:b1	14:4
///
ENTRY	W4079_9	Glycan
NODE	17
	1	glcnac	-24.0	-22.0
	2	neuac	-48.0	-16.0
	3	gal	-40.0	-16.0
	4	glcnac	-32.0	-16.0
	5	gal	-40.0	-10.0
	6	glcnac	-32.0	-8.0
	7	man	-24.0	-12.0
	8	glcnac	-32.0	-2.0
	9	man	-24.0	-2.0
	10	man	-16.0	-12.0
	11	glcnac	-8.0	-12.0
	12	fuc	-8.0	12.0
	13	glcnac	0.0	-0.0
	14	glcnac	-33.0	6.0
	15	gal	-40.0	-2.0
	16	neuac	-49.0	-10.0
	17	fuc	-39.0	4.0
EDGE	16
	1	11:b1	13:4
	2	10:b1	11:4
	3	1:b1	10:4
	4	7:a1	10:3
	5	4:b1	7:2
	6	3:b1	4:4
	7	2:a2	3:3/6
	8	6:b1	7:4
	9	5:b1	6:4
	10	16:a2	5:3/6
	11	9:a1	10:6
	12	8:b1	9:2
	13	15:b1	8:4
	14	17:a1	8:3
	15	14:b1	9:6
	16	12:a1	13:6
///
ENTRY	W4212_9	Glycan
NODE	18
	1	neuac	-48.0	-20.0
	2	gal	-40.0	-20.0
	3	glcnac	-32.0	-20.0
	4	neuac	-48.0	-16.0
	5	gal	-40.0	-16.0
	6	glcnac	-32.0	-16.0
	7	man	-24.0	-18.0
	8	gal	-40.0	-6.0
	9	glcnac	-32.0	-6.0
	10	gal	-40.0	-0.0
	11	glcnac	-32.0	2.0
	12	man	-24.0	-2.0
	13	man	-16.0	-10.0
	14	glcnac	-8.0	-10.0
	15	glcnac	0.0	-0.0
	16	fuc	-16.0	6.0
	17	fuc	-37.0	12.0
	18	fuc	-39.0	-11.0
EDGE	17
	1	14:b1	15:4
	2	13:b1	14:4
	3	7:a1	13:3
	4	3:b1	7:2
	5	2:b1	3:4
	6	1:a2	2:3/6
	7	6:b1	7:4
	8	5:b1	6:4
	9	4:a2	5:3/6
	10	12:a1	13:6
	11	9:b1	12:2
	12	8:b1	9:4
	13	18:a1	9:3
	14	11:b1	12:6
	15	10:b1	11:4
	16	17:a1	11:3
	17	16:a1	15:6
///
ENTRY	W4645_2	Glycan
NODE	19
	1	neuac	-48.0	-16.0
	2	gal	-40.0	-16.0
	3	glcnac	-32.0	-16.0
	4	gal	-40.0	-10.0
	5	glcnac	-32.0	-8.0
	6	man	-24.0	-12.0
	7	gal	-40.0	-4.0
	8	glcnac	-32.0	-2.0
	9	man	-24.0	-2.0
	10	man	-16.0	-12.0
	11	glcnac	-8.0	-12.0
	12	fuc	-8.0	12.0
	13	glcnac	0.0	-0.0
	14	glcnac	-22.0	-19.0
	15	neuac	-50.0	-9.0
	16	neuac	-51.0	-4.0
	17	glcnac	-33.0	5.0
	18	gal	-42.0	6.0
	19	fuc	-36.0	15.0
EDGE	18
	1	11:b1	13:4
	2	10:b1	11:4
	3	6:a1	10:3
	4	3:b1	6:2
	5	2:b1	3:4
	6	1:a2	2:3/6
	7	5:b1	6:4
	8	4:b1	5:4
	9	15:a2	4:3/6
	10	9:a1	10:6
	11	8:b1	9:2
	12	7:b1	8:4
	13	16:a2	7:3/6
	14	17:b1	9:6
	15	18:b1	17:4
	16	19:a1	17:3
	17	14:b1	10:4
	18	12:a1	13:6
///
ENTRY	W4835_9_2	Glycan
NODE	21
	1	gal	-40.0	-20.0
	2	glcnac	-32.0	-20.0
	3	gal	-40.0	-16.0
	4	glcnac	-32.0	-16.0
	5	man	-24.0	-18.0
	6	gal	-40.0	-6.0
	7	glcnac	-32.0	-6.0
	8	gal	-42.0	1.0
	9	glcnac	-32.0	2.0
	10	man	-24.0	-2.0
	11	man	-16.0	-10.0
	12	glcnac	-8.0	-10.0
	13	glcnac	0.0	-0.0
	14	glcnac	-49.0	-4.0
	15	gal	-58.0	-4.0
	16	neuac	-54.0	-21.0
	17	neuac	-54.0	-16.0
	18	fuc	-6.0	11.0
	19	fuc	-38.0	12.0
	20	fuc	-40.0	-9.0
	21	fuc	-40.0	-12.0
EDGE	20
	1	12:b1	13:4
	2	11:b1	12:4
	3	5:a1	11:3
	4	2:b1	5:2
	5	1:b1	2:4
	6	16:a2	1:3/6
	7	4:b1	5:4
	8	3:b1	4:4
	9	17:a2	3:3/6
	10	10:a1	11:6
	11	7:b1	10:2
	12	6:b1	7:4
	13	14:b1	6:3
	14	15:b1	14:4
	15	20:a1	7:3
	16	9:b1	10:6
	17	8:b1	9:4
	18	19:a1	9:3
	19	18:a1	13:6
	20	21:a1	4:3
///
			   </textarea><BR>
			   Or load KCF from a file: <BR>
			   <input name="controlFILE" type="file">
        		</TD>
  		     </tr>

  		     <tr>
                     </tr>

	       <tr>
		  <TD colspan="5" alighn="center">
    		     <fieldset>
			<legend>option</legend>
			<table>
			   <tr>
			      <TD class = "bgc_glay"><B><FONT size = "3">Subtree size </FONT></B>
			         <div><FONT size = "2">Specify the range of subtree size (the number of<br>
					 				       monosaccharides) from one to nine for breaking<br>
										   down glycan structures.
					 </FONT></div>
			      </TD>
      	  		      <TD colspan ="2">
			         min&nbsp<input type = "text" name = "min" value = "2" size = "3">
				 &nbsp <= &nbsp size &nbsp <= &nbsp 
				 max&nbsp<input type = "text" name = "max" value = "5" size = "3">
			      </TD>
			   </tr>
			   <!--tr>
			      <TD class = "bgc_glay"><B><FONT size = "3">Glycan Score Matrix</FONT></B>
			         <div><FONT size = "2">Choose a set of glycan score matrix.<br>
				 The score matrix is used for the similarity calculation.</FONT></div>
			      </TD>
      	  		      <TD colspan ="2">
EOF
 if($id == 0 || $id eq ""){
    print "<select name = \"scorematrix\">";
    print "<option value = \"0\">Non</option>";
    print "<option value = \"1\">N-glycan</option>";
    print "<option value = \"2\">O-glycan</option>";
    print "<option value = \"3\">Sphingolipid</option>";
    print "</select>";
 }elsif($id > 0){
    print "<select name = \"scorematrix\">";
    print "<option value = \"0\">Non</option>";
    print "<option value = \"1\">N-glycan</option>";
    print "<option value = \"2\">O-glycan</option>";
    print "<option value = \"3\">Sphingolipid</option>";

    my $num = 0;
    @result = Get_User_Data_List($DBH, $id, 'Score Matrix Tool');
    foreach my $line(@result)
    {
       $file_id = @$line[0];
       $scmatName = @$line[1];
       print "<option value = \"$file_id\">$scmatName</option>";
    }

    print "</select>";
 }

print <<EOF;
			      </TD>
			   </tr-->
      			</table>
			   </tr>
      			</table>
		     </fieldset>
		  </TD>
	       </tr>

               <tr>
                  <TD colspan = "3" align = "center">
                     <input value="reset" onClick="KERNEL.classname.value='',KERNEL.target.value='',KERNEL.control.value='',KERNEL.min.value='',KERNEL.max.value='',KERNEL.type.value='',KERNEL.scorematri.value=''" type="reset">
                     <input value="run" type="submit">
                  </TD>
               </TBODY>
            </TABLE>
         </form>
      </td>
      <table width=800>
         <caption>As an estimate of computation time, this table lists the minimum and maximum data sizes allowed and their corresponding computation times.</caption>
         <tr bgcolor="#F5F5F5">
            <th></th>  <th>data size</th>  <th>computation time</th>
         </tr>
         <tr align=center bgcolor="#F5F5F5">
            <td>minimum</td>  <td>10 entries per data set</td>  <td>1 minute</td>
         </tr>
         <tr align=center bgcolor="#F5F5F5">
            <td>maximum</td>  <td>400 entries per data set</td>  <td>4 hours</td>
         </tr>
      </table>
   </body>
</html>
EOF
