require 'pp'
$sugarHash = Hash.new
$bondHash = Hash.new
$edge_nodeHash = Hash.new{|h,key| h[key] = []}
$node_edgebondHash = Hash.new
$l_kcf = Array.new
$r_kcf = Array.new
$layerHash = Hash.new
$xHash = Hash.new
$yHash = Hash.new
$pHash = Hash.new
$cHash = Hash.new{|h,key| h[key] = []}
$rootnode = -1

 ####### makehash (decide root & make l_kcl and r_kcf) ######

 def makeHash(kcf)

	mode = 0
	kcfArray = kcf.split(/\n/)
	kcfArray.each { |line|
	   
	   if line =~ /^NODE/
		mode = 1 
	   elsif line =~ /^EDGE/	
    		mode =2
	   elsif line =~ /^BRACKET/
		mode = 3
	   end

		if  line =~ /^\s+(\d*)\s+(.+)\s+(-?\d+\.?\d?)\s+(-?\d+\.?\d?)/ && mode == 1
 a = $1
 b = $2
 c = $3
 d = $4
			b.gsub!(/ /,"")
			$sugarHash.store(a.to_i,b.downcase)
			$xHash.store(a.to_i,c.to_f)
			$yHash.store(a.to_i,d.to_f)

				if ($rootnode == -1 || c.to_f > $xHash[$rootnode]) || (c.to_f == $xHash[$rootnode] && d.to_f > $yHash[$rootnode])
		   $rootnode = a.to_i
	        		end 

		elsif  line =~ /^\s+(\d+)\s+(\d+):?(.*)\s+(\d+):?(.*)/ && mode == 2
 e = $1
 f = $2
 g = $3
 h = $4
 j = $5
			g.gsub!(/ /,"")
			j.gsub!(/ /,"")
			bond = "["+g+"-"+j+"]"
			$bondHash.store(e.to_i,bond)

			$edge_nodeHash[e.to_i] << f.to_i
			$edge_nodeHash[e.to_i] << h.to_i

			$l_kcf << f.to_i
			$r_kcf << h.to_i
		end
	}
 end

 #####  ##### decide root & make l_kcf and r_kcf #### #####


 ######## make pHash cHash ########

	def makepcHash(kid)
		if $r_kcf.include?(kid)

		$r_kcf.each_with_index{ |item, idx|

			if item == kid

			pnode = kid
			cnode = $l_kcf[idx]

			$pHash[cnode] = pnode
			$cHash[pnode] << cnode
			$l_kcf[idx] = 0
			$r_kcf[idx] = 0

			makepcHash(cnode)
			end
		}
		end

		if $l_kcf.include?(kid)

			$l_kcf.each_with_index{ |item2, idx2|

				if item2 == kid
				pnode = kid
				cnode = $r_kcf[idx2]

				$pHash[cnode] = pnode
				$cHash[pnode] << cnode
				$l_kcf[idx2] = 0
				$r_kcf[idx2] = 0

				makepcHash(cnode)
				end
			}
		end
	end

 ###  ### make phash chash ###  ###


 ####### makeLayer ########

   def makeLayer(n,layer)

	$layerHash[n] = layer

	if $cHash.key?(n)

	    children = $cHash[n]
		children.each { |child|
	    	    makeLayer(child,layer+1)
	 	}
	end
   end

 ###  ## makeLayer_fin ##  ###


 ############### checkHash ###############

   def checkHash

	if (($sugarHash.size - $layerHash.size) >= 1) && $pHash.value?($rootnode)

		for i in 1..$sugarHash.size

           		unless $layerHash.key?(i)

			p = $cHash[i]
	       		$cHash.delete(i)
	       		$pHash.delete(p[0])
		
	       		$pHash[i] = p[0]
	       		$cHash[p[0]] << i

		 		$cHash.each { |parent,c|

			  		c.each { |cc|
					    if cc == p[0]
						$pHash[p[0]] = parent
					    end
			  		}
		 		 }

			$pHash.each { |ch,pa|
				if !$cHash.key?(pa) || !$cHash[pa].include?(ch)
					$cHash[pa] << ch
				end
			}
	     	end
	end

      	elsif (($sugarHash.size - $layerHash.size) > 1) && !$pHash.value?($rootnode)

		unless $pHash.value?($rootnode)

		ch_c = $cHash[$rootnode]

		ph_p = $pHash[ch_c[0]]
		$pHash.delete(ch_c[0])
		$pHash[ph_p] = ch_c[0]
		$pHash[ch_c[0]] = $rootnode

			$cHash.each { |oya,ko|

				if ko.include?(ch_c[0])

					if $cHash[ph_p].include?(ch_c[0])
					$cHash[ph_p].delete(ch_c[0])
					$cHash[ch_c[0]] << ph_p
					end
				end

					ko.each { |koko|
					phashp = $pHash[koko]

			     			unless  phashp == oya
			     			$pHash.delete(koko)
			     			$cHash.delete(phashp)

			     			$pHash[phashp] = koko
			     			$pHash[koko] = oya
			     			$cHash[koko] << phashp
			     			end 
					}
			}
		end
     	end

   end

 ###  ### checkhash_fin ###  ###


 ############# make qgram #############

def makeqgramHash(qvalmin, qvalmax)
$qgramHash = Hash.new{|q,key| q[key] = Array.new}
qArray = Array.new
tmpArray = Array.new
qArray2 = Array.new

## q=1 ##

    	$sugarHash.each{ |n,value|
    		qArray = Array.new
    		qArray << n
    		$qgramHash[1] << qArray
	}

## qgram  from 2 to 9 ##

	#if $sugarHash.size > 9
	if $sugarHash.size >= qvalmin
	        
	    if qvalmax > $sugarHash.size
	        qvalmax = $sugarHash.size
	    end

	    for i in 2..qvalmax

		qArray2 = $qgramHash[i-1]
		qArray = Array.new
		tmpArray = Array.new

		qArray2.each { |q|

			q.each { |x|

		   	child = $cHash[x]

				child.each{ |c|
		 	   	tmpArray = q.dup

			   		if !tmpArray.include?(c)
			      		tmpArray << c
			      		tmpArray.sort!

		         			if !qArray.include?(tmpArray)
				   		qArray << tmpArray
						end

			   		end
				}
	    		}
		}
   		$qgramHash[i] = qArray
		#p qArray
	    end
        end

	###### bug test ######

	#if $layerHash.size != $sugarHash.size
   	  #p "[layer!=sugar]",$name,"\n"
        #  next
	#end

     	#if $pHash.size != $sugarHash.size-1
   	  #p "[pHash!=sugar-1]",$name,"\n"
	#  next
     	#end

	#if qvalmax > 1 && $qgramHash[2].size != $bondHash.size
     	  #p "[qgram2!=bond]",$name,"\n"
	  #exit
	  #next
	#end

	###  ## bug test_fin ## ###
end

 ######  #### makeqgramHash_fin ####  #####


 ############ make hash key is node,value is bond & sugar #############

def make_node_edge_bondHash

	$node_edgebondHash[$rootnode] = $sugarHash[$rootnode]

	$pHash.each{|c,p|

		$edge_nodeHash.each{|edge,nodes|

		 	if nodes.include?(c) && nodes.include?(p)

			e_node = c

			   edge_bond = $bondHash[edge] + $sugarHash[e_node]
			   $node_edgebondHash[e_node] = edge_bond
		    	end
		}
	}
end

 ####  #### make hash key is node,value is bond & sugar ####  #### 


 ########## make qgram with bunki #########
 
def makeqgram_with_bunki(qvalmin, qvalmax)

	qgrams = Array.new

	$qgramHash.keys.sort.each {|i|
	
		$qgramHash[i].sort.each { |ii|

			lowest_layer = 100
			ii.sort.each { |iii|

				iii_layer = $layerHash[iii]

				if iii_layer < lowest_layer
					lowest_layer = iii_layer
					$lowest_node = iii
				end
			}
			a = lowest_layer.to_s + "+"
			a = makebunki($lowest_node,a,ii)
			if i >= qvalmin && i <= qvalmax
				#p a
				qgrams << a
			end
		}	
	}

	qgrams.sort.each {|x| p x}
end

 #######  ######## make qgram with bunki_fin ######  ######


 ######### makebunki #########

def makebunki(s_root,a,ii)

	a += $node_edgebondHash[s_root]

	if (ii & $cHash[s_root]).length == 1

	kyotuu = ii & $cHash[s_root]
	a = makebunki(kyotuu[0],a,ii)
	end

	if (ii & $cHash[s_root]).length > 1

			$cHash[s_root].each { |c|

				if ii.include?(c)
				a += "<"
				a = makebunki(c,a,ii)
				a += ">"
				end
			}
	end
  return a
end

 ###  ### makebunki_fin ###  ###



######################## main ###########################

#require "rings"
#r = Rings::new
#my = r.connectToRings()
#   gNum = "G"

#   glycanID = r.getGlycanIDsFromGname(my,gNum)
   
#	glycanID.each { |i|
#		$name = r.getGnameFromGlycanID(my,i)
		#print "[Entry]",i,"\t",$name,"\n"

#		kcf = r.getEditedKCFFromGname(my,i)
		#p "KCF=",kcf, "\n"

kcf = ""
filename = ARGV[0]
file = open(filename)
while line = file.gets
	if line =~ /ENTRY\s+(.+)\s+Glycan/
		name = $1
#print "[Entry]",$1,"\n"
		names = name.split(" ")
		print names[0],"\n"
		#$Gnum = $1
		kcf = kcf + line


	elsif line =~ /\/\/\//
		kcf = kcf + line
		kcf.chomp

$sugarHash = Hash.new
$bondHash = Hash.new
$edge_nodeHash = Hash.new{|h,key| h[key] = []}
$node_edgebondHash = Hash.new
$layerHash = Hash.new
$l_kcf = Array.new
$r_kcf = Array.new
$xHash = Hash.new
$yHash = Hash.new
$pHash = Hash.new
$cHash = Hash.new{|h,key| h[key] = []}
$rootnode = -1

		makeHash(kcf)

			makepcHash($rootnode)
			makeLayer($rootnode,0)

	 	while $sugarHash.size != $layerHash.size do
			checkHash
     			makeLayer($rootnode,0)
 		end

			makeqgramHash(ARGV[1].to_i,ARGV[2].to_i)
			make_node_edge_bondHash

			makeqgram_with_bunki(ARGV[1].to_i,ARGV[2].to_i)
			#print "///\n";

		kcf = ""
		next
	else
		kcf = kcf + line
	end
end
file.close
#	}


####  ####  ####  #### main_fin #####  #####  ####  #####

