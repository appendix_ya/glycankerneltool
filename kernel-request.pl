#! /usr/bin/perl

use RINGS::Web::user_admin;
use RINGS::Web::HTML;
use RINGS::Tool;
use RINGS::Glycan::KCF;
use RINGS::Glycan::Mapping;
use File::Find;
use File::Copy;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use File::Copy;
use warnings;
require "../share/cgi-lib.pl";
$CGI::POST_MAX = 1024 * 1024 * 16;       # 1024 * 1KBytes = 1MBytes.



# ===================
# Global Parameters
# ===================
 $CLASSNAME = "";
 $TGLYCAN = "";     # Target KCF data: label=1
 $CGLYCAN = "";     # Control KCF data: label=0
 $SUBTREE_MIN = "";	#subtree parameter (min)
 $SUBTREE_MAX = "";	#subtree parameter (max)
 $ALPHA = "";
 $METHOD_NUMBER= "";        # weighted (LK) :1, bio-weighted (LKR) :2, both:3
 $SCORE_MATRIX= "";

 $DBH = Connect_To_Rings;
 $TIME = &date_data;	#get time:YYMMDDHHMMSS+rand*3
 $SAVE_DIR = "/tmp/kernel/$TIME";
 $STATUS = 1;	#calculation status: 1)during calculation, 2)Error, 3)Finish calculation



#============
# Get User ID
#============
 $user_id = Get_Cookie_Info();
 my $input_data_size = 0;




# ===================
# Output Header
# ===================
print "Content-type: text/html\n\n";
print "<html>\n";
print "<head>\n";
my $title = "Kernel Training: Accepting your request ...";
my $head = Make_Head_From_Title($title);
print "$head\n";
my $h1 = Make_H1_From_Header($title);
print "$h1";
print "<br>\n";






# ==================
# Get form data
# ==================

if(-d "/tmp/kernel/"){	#classname check
   &makeDir;	#make $TIME directory under /tmp/kernel/, and copy and unzip Matlab compile file
}else{
   mkdir("/tmp/kernel/", 0777) or die "Making directory failed: $!";
   &makeDir;	#make $TIME directory under /tmp/kernel/, and copy and unzip Matlab compile file
}

my @postdatas = get_post_data();
$CLASSNAME = $postdatas[0];
$TGLYCAN = $postdatas[1];  	#KCF data
$CGLYCAN = $postdatas[2]; 
$SUBTREE_MIN = $postdatas[3];	#from 1 to 9
$SUBTREE_MAX = $postdatas[4];
$METHOD_NUMBER = 2; 	#1:weighted, 2:bio-weighted, 3:both
#$SCORE_MATRIX = $postdatas[6]; #default: 0=non, 1=N-glycan, 2=O-glycan, 3=Sphingolipid, other number = user's file directory number
$SCORE_MATRIX = 0;

$ALPHA = 0.35;

print "<input type=\"hidden\" name=\"sys_1\" value=\" ";
print "\"><BR>";



# File lists ==========
$CKCFFILE = "$SAVE_DIR/control.txt";
$TKCFFILE = "$SAVE_DIR/target.txt";	
$WEIGHTSFILE = "$SAVE_DIR/weights.txt";
$LABELSFILE = "$SAVE_DIR/labels.txt";
$PARAMFILE = "$SAVE_DIR/param.txt";		#parameter: alpha, method type, minimum size, maximum size and calculation ID
$SCOMATFILE = "$SAVE_DIR/score_matrix.txt";	#glycan score matrix file





################################
#            Main              #
################################
##### main excute #####

 &prepare_source_files;
 my $error = "";

 for (my $i=$SUBTREE_MIN; $i<=$SUBTREE_MAX; $i++){
    my $qFileT = "$SAVE_DIR/target-$i.txt";
    my $qFileC = "$SAVE_DIR/control-$i.txt";

    if (-f $qFileT && -f $qFileC){
       my $file_sizeT = -s $qFileT;
       my $file_sizeC = -s $qFileC;
       if ($file_sizeT = 0 || $file_sizeC = 0){
          $error .= "Can not create subtree file: subtree = $i<br>";
          $error .= "The size of entry nodes should be larger than $i or the size of subtree should be smaller than $i.<BR>";
       }
    }else{
       $error .= "Does not exist subtree file: subtree = $i<br>";
    }

    if($error ne ""){
       &printHeader("ERROR: $error");
       exit;
    }
 }

 my $data_id = Insert_Kernel_Data($DBH, $CLASSNAME, $ALPHA, $SUBTREE_MIN, $SUBTREE_MAX, $TIME, $input_data_size, 0, $user_id);

 #=======================
 # User management system
 #=======================

 if ($user_id != 0) {
    $SAVE_USER_DIR = "/var/www/userdata/$user_id";
    Make_Dir($SAVE_USER_DIR);
    $SAVE_USER_DIR = "$SAVE_USER_DIR/KernelTool";
    Make_Dir($SAVE_USER_DIR);
    $SAVE_USER_DIR = "$SAVE_USER_DIR/$data_id";
    Make_Dir($SAVE_USER_DIR);

    copy($CKCFFILE, $SAVE_USER_DIR."/control.txt");
    copy($TKCFFILE, $SAVE_USER_DIR."/target.txt");
    copy($PARAMFILE, $SAVE_USER_DIR."/param.txt");
    copy($SCOMATFILE, $SAVE_USER_DIR."/score_matrix.txt");
 }


 #-------------------------------------------
 # input calculation status in RINGS Database
 # $TIME: status id (time + randam number)
 # $STATUS: 1)during calculation, 2)ERROR, 3)Finish calculation
 # $METHOD_NUMBER: 1)weighted q-gram (LK), 2)bio-weighted q-gram (LKR), 3)both
 #-------------------------------------------
 Inst_K($DBH, $TIME, $STATUS, $METHOD_NUMBER, $ALPHA, $CLASSNAME, $SUBTREE_MIN, $SUBTREE_MAX, $data_id);
 &output;

 `sh $SAVE_DIR/run_Kernel_Comp.sh /opt/MatLab/ $TIME`;

exit;
#---------------------------------------------------------------------------------------




#########################
# Get time data		#
#########################
sub date_data{
 my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime(time);
 $mon = $mon+1;
 my $time = sprintf("%02d%02d%02d%02d%02d",$mon,$mday,$hour,$min,$sec);
 $year = $year+1900;
 if($year =~ /\d\d(\d\d)/){
     $year = $1;
 }
 $time = $year.$time;

 my $rand = sprintf("%03d",int(rand(1000)));
 my $id = $time.$rand;

 return ($id);
}



#########################
# make Directory 	#
#########################
sub makeDir{
 if(-d "$SAVE_DIR"){	#classname check
    die "already we have $TIME directory, please try later: $!";
 }else{
    mkdir("$SAVE_DIR", 0777) or die "Making directory failed: $!";
    mkdir("$SAVE_DIR/Feature/", 0777) or die "Making directory failed: $!";
    mkdir("$SAVE_DIR/Featurelist/", 0777) or die "Making directory failed: $!";
    mkdir("$SAVE_DIR/ROC/", 0777) or die "Making directory failed: $!";
    mkdir("$SAVE_DIR/Kernel/", 0777) or die "Making directory failed: $!";
    mkdir("$SAVE_DIR/ScoreMatrix/", 0777) or die "Making directory failed: $!";

    system("cp /matlab_program/Linkagesimi.mat $SAVE_DIR") and die "Can not copy M file: $!";
    system("cp -r /matlab_program/Kernel_Comp $SAVE_DIR") and die "Can not copy M.zip file: $!";
    system("cp /matlab_program/run_Kernel_Comp.sh $SAVE_DIR") and die "Can not copy Matlab run-file: $!";
 }
}



#########################
#   Reform KCF file	#
#########################
sub prepare_source_files {

 # ==============================================
 # get data from user-input data and reform them
 # ==============================================
 $num_gIDs = 0;
 my $gName = 0;

 $input_data_size = 0;

 # Target Glycan Data ================
 if(!open(DIR1, $TKCFFILE)){  #classname check
    ($gIDs, $TGLYCAN, $gName) = &reformKCF($TGLYCAN, $gName);
    $num_g = @{$gIDs};
    $num_gIDs = $num_g;
    open(OUT, ">$TKCFFILE");
       print OUT "$TGLYCAN"; #file $tglycan
    close(OUT);
    if ($user_id != 0) {
        $input_data_size += -s $TKCFFILE;
    }
 }else{
    &printHeader("ERROR: Please try later.\n");
    close(DIR1);
    exit;
 }
	
 # Control Glycan Data =================
 if(!open(DIR1, $CKCFFILE)){  #classname check
    ($gIDs_N, $CGLYCAN, $gName) = &reformKCF($CGLYCAN, $gName);
    $num_g = @{$gIDs_N};
    $num_gIDs = $num_gIDs + $num_g;
    foreach(@{$gIDs_N}){
       push(@{$gIDs},$_);
    }
    open(OUT, ">$CKCFFILE");
       print OUT "$CGLYCAN"; #file $cglycan
    close(OUT);
    if ($user_id != 0) {
        $input_data_size += -s $CKCFFILE;
    }
 }else{
    &printHeader("ERROR: Please try later.\n");
    close(DIR1);
    exit;
 }
	

 # Weight Data =================
 if(!open(DIR1, $WEIGHTSFILE)){  #classname check
    $WEIGHT = &createWEIGHT(\@{$gIDs}, $WEIGHT);
    open(OUT, ">$WEIGHTSFILE");
    print OUT "$WEIGHT\n"; #file $cglycan
    close(OUT);
 }else{
    &printHeader("ERROR: Please try later.\n");
    close(DIR1);
    exit;
 }



 #=============
 # make files 
 #=============
 my $num = &labels_file();
 if($num < 20){
    my $erMes = "ERROR: Please input more than 10 entries as target and control data.\n";
    &printHeader($erMes);
    exit;
 }

 system("chmod 0777 $SAVE_DIR");
 system("chmod 0777 $SAVE_DIR/*");

# system("sh getqgrams.sh $SAVE_DIR/target.txt $SUBTREE_MIN $SUBTREE_MAX");
# system("sh getqgrams.sh $SAVE_DIR/control.txt $SUBTREE_MIN $SUBTREE_MAX");
 system("sh getqgrams.sh $SAVE_DIR/target.txt 1 $SUBTREE_MAX");
 system("sh getqgrams.sh $SAVE_DIR/control.txt 1 $SUBTREE_MAX");

 my $maxLsize = 0;
 my $maxLsizeT = &check_qFile("target");
 my $maxLsizeC = &check_qFile("control");

 if($maxLsizeT > $maxLsizeC){
    $maxLsize = $maxLsizeT;
 }else{
    $maxLsize = $maxLsizeC;
 }
 &param_file($ALPHA, $METHOD_NUMBER,$SUBTREE_MIN,$SUBTREE_MAX, $TIME, $maxLsize);

 
 my $scomat_data = "/var/www/userdata/".$user_id."/ScoreMatrixTool/".$SCORE_MATRIX."/result.txt";
 open(FH1, "> $SCOMATFILE");
    open(FH2, $scomat_data);
    while(my $line = <FH2>){
       if($line =~ /^[^\s<]/){
	  $line =~ s/:/ : /g;
       }
       print FH1 "$line";
    }
    close (FH2);
 close (FH1);
}




##################
# Get post data  #
##################
sub get_post_data {
 $formdata = new CGI;
 $formdata->param;  #error processing
 die($formdata->cgi_error) if ($formdata->cgi_error);

 $maxsize = 10000;
 my $size = (stat($formdata))[7];
 if($size > $maxsize * 1024 * 16){
    print "The input size is too large. Max $maxsize kB";
 }

 my $classname = $formdata->param('classname');  #Get Class Name
 my $tglycan = $formdata->param('target');     #Get Target 
 my $cglycan = $formdata->param('control');     #Get Control 
 my $min = $formdata->param('min');              #Get Minimum
 my $max = $formdata->param('max');              #Get Maximum
 my $type = $formdata->param('type');       	#Get Method number
 my $tglycanFILE = $formdata->param('targetFILE');     #Get Target FILE 
 my $cglycanFILE = $formdata->param('controlFILE');     #Get Control FILE
 my $score_matrix = $formdata->param('scorematrix');     #Get Score Matrix ID 


 # ===================================
 # Error and Default value Processing
 # ===================================
 $error = "";

 if($classname eq ""){
    $error .= "Please input Data set Name.<br>";
 }elsif($classname =~ / /){
   $error .= "Please use underscore ('_') instead of space (' ') for Data set Name.<br>";
 }

 if($tglycan eq "" && $tglycanFILE eq ""){
    $error .= "Please input Target Data.<br>";
 }elsif($tglycanFILE ne ""){
    my $tmpTF = "$SAVE_DIR/$tglycanFILE";
    copy($tglycanFILE, $tmpTF);

    if(!open(TUPFILE, $tmpTF)){
       $error .= "Unable to open file $tglycanFILE.<br>";
    }else{
       open(TUPFILE, $tmpTF);
       $buf = "";
       $tglycan = "";
       while (read (TUPFILE, $buf, 8192)){
          $buf =~ s/</&lt;/g;
          $buf =~ s/>/&gt;/g;

          $tglycan .= $buf;
       }
       close (TUPFILE);
    }
 }

 if($cglycan eq "" && $cglycanFILE eq ""){
    $error .= "Please input Control Data.<br>";
 }elsif($cglycanFILE ne ""){
    my $tmpCF = "$SAVE_DIR/$cglycanFILE";
    copy($cglycanFILE, $tmpCF);

    if(!open(CUPFILE, $tmpCF)){
       $error .= "Unable to open file $cglycanFILE.<br>";
    }else{
       open(CUPFILE, $tmpCF);
       $buf = "";
       $cglycan = "";
       while (read (CUPFILE, $buf, 8192)){
          $buf =~ s/</&lt;/g;
          $buf =~ s/>/&gt;/g;

          $cglycan .= $buf;
       }
       close (CUPFILE);
    }
 }


 if($min eq ""){
    $min = 1;
 }elsif($min =~ /\d\d/){
    $error .= "Please input the minimum size of subtree from one to eight.<br>";
 }
 if($max eq ""){
    $max = 9;
 }elsif($max =~ /\d\d/){
    $error .= "Please input the maximum size of subtree from two to nine.<br>";
 }
 if($error ne ""){
    &printHeader("ERROR: $error");
    exit;
 }

 my @postdatas = ($classname, $tglycan, $cglycan, $min, $max, $type, $score_matrix);
 @postdatas;
}





#############################################################################
#  Reform the KCF: arrange spaces and add Glycan Entry if they do not have  #
#############################################################################
sub reformKCF {
 my ($GLYCAN_DATA, $gName) = @_;

 my @kcf_line = split(/\n/, $GLYCAN_DATA);
 my @gIDs = ();
 my $a_line = "";
 my $glycan_sub = "";
 my $gID = "";
 my $mode = 0;

 foreach my $line (@kcf_line){
    $line =~ s/^\s+//g;
    $line =~ s/\r//g;
    my @items = split(/\s+/, $line);
    my $num_items = @items;
    if ($num_items >= 1){
       if($items[0] =~ /ENTRY/){
 	  $mode = 1;
       }elsif($items[0] =~ /NODE/){
	  $mode = 2;
       }elsif($items[0] =~ /EDGE/){
	  $mode = 3;
       }elsif($items[0] =~ /\/\/\//){
	  $mode = 4;
       }


       if($mode == 1 && $items[0] =~ /ENTRY/i){
	  if($items[1] =~ /GLYCAN/i){
	     $items[0] = $items[0]."\tG".sprintf("%03d",$gName);
	     $gID = "G".sprintf("%03d",$gName);
	     push(@gIDs, $gID);
	     $gName = $gName+1;
	  }else{
	     $items[1] =~ s/[\.\/]/_/g;
 	     $gID = $items[1];
	     push(@gIDs, $gID);
	  }
          $a_line = join("\t", @items);
          if($glycan_sub eq ''){
	     $glycan_sub = $a_line;
          }else{
	     $glycan_sub = $glycan_sub."\n".$a_line;
          }
       }elsif($mode == 2 && $items[0] =~ /NODE/){
          $a_line = join("\t", @items);
          $glycan_sub = $glycan_sub."\n".$a_line;
       }elsif($mode == 2 && $num_items == 4){
          if($items[2] !~ /\.\d/){
	     $items[2] = $items[2]."\.0";
	     $items[3] = $items[3]."\.0";
          }
	  $items[1] = lc($items[1]);
          $a_line = join("\t", @items);
          $glycan_sub = $glycan_sub."\n"."\t".$a_line;
       }elsif($mode == 2 && $num_items != 4){
          &printHeader("ERROR: The KCF of $gID is not correct. <BR>Please check the format of NODE.");
          exit;
       }elsif($mode == 3 && $items[0] =~ /EDGE/){
          $a_line = join("\t", @items);
          $glycan_sub = $glycan_sub."\n".$a_line;
       }elsif($mode == 3 && $num_items == 3){
 	  $items[1] = lc($items[1]);
          my $a_line = join("\t", @items);
          $glycan_sub = $glycan_sub."\n"."\t".$a_line;
       }elsif($mode == 3 && $num_items != 3){
          &printHeader("ERROR: The KCF of $gID is not correct. <BR>Please check the format of EDGE.");
          exit;
       }elsif($mode ==4 && $items[0] =~ /^\//){
          $glycan_sub = $glycan_sub."\n".$items[0];
       }
    }
 }
 return(\@gIDs, $glycan_sub, $gName);
}




##############################################
# make weight file, here, all weights are 1  #
##############################################
sub createWEIGHT{
 my ($gIDs, $WEIGHT_DATA) = @_;

 my @GlycanIDs = @{$gIDs};
 my $weight_2 = "";
 foreach my $item (@GlycanIDs){
    my $a_line = $item."\t1";
    if($weight_2 eq ''){
       $weight_2 = $a_line;
    }else{
       $weight_2 = $weight_2."\n".$a_line;
    }
 }
 return($weight_2);
}




############################
#    Create label file     #
############################
sub labels_file {
 open(F1,"> $LABELSFILE");
	
 #======= Target ======== 
 open(FILE_T, $TKCFFILE);
 while(my $line_t = <FILE_T>){
    if($line_t =~ /^ENTRY\s+(\S+)(\s+Glycan)?/){
       my $entry = $1;
       $entry =~ s/[\.\/]/_/g;
       print F1 "$entry\t1\n";
       push(@num_glycans, $entry);
       $line_t =~ s/[\.\/]/_/g;
    }
 }
 close(FILE_T);

 #======= Control =========
 open(FILE_C, $CKCFFILE);
 while(my $line_c = <FILE_C>){
    if($line_c  =~ /^ENTRY\s+(\S+)(\s+Glycan)?/){
       my $entry = $1;
       $entry =~ s/[\.\/]/_/g;
       print F1 "$entry\t0\n";
       push(@num_glycans, $entry);
       $line_c =~ s/[\.\/]/_/g;
    }
 }
 close(FILE_C);
 close(F1);
 my $num = @num_glycans;
 return($num);
}



############################
#    Create label file     #
############################
sub param_file {
 my ($alpha, $param, $min, $max, $time, $maxL) = @_;
 open(F1,"> $PARAMFILE");
    print F1 "$alpha\t$param\t$min\t$max\t$time\t$maxL";
 close(F1);
}




#########################
#    Check q-file       #
#########################
sub check_qFile{
 my $file = shift @_;

 for(my $i=$SUBTREE_MIN; $i<=$SUBTREE_MAX; $i++){
    my $qFile = "$SAVE_DIR/$file-$i.txt";
    if (-f $qFile){
       my $file_size = -s $qFile;
       if ($file_size == 0){
          $error .= "Can not create subtree file: subtree = $i ($file data) <br>";
          $error .= "The size of entry nodes should be larger than $i or the size of subtree should be smaller than $i.<BR>";
       }else{
          open(FH,$qFile);
          my $q_check = -1;

          while(my $line = <FH>){
             if($line =~ /^\"\d/){
                $q_check = 1;
             }
          }
          close(FH);

          if ($q_check == -1){
             $error .= "Can not create subtree file: subtree = $i ($file data)<br>";
             $error .= "The size of entry nodes should be larger than $i <BR>or the size of subtree should be smaller than $i.<BR>";
             $error .= "Please use \"option\" of Glycan Kernel Tool Entry to avoid this error.<BR>";
          }
       }
    }else{
       $error .= "Does not exist subtree file: subtree = $i ($file data)<BR>";
    }
    if($error ne ""){
       &printHeader("ERROR: $error");
       exit;
    }
 }
 
 my $maxLF = "$SAVE_DIR/$file-1.txt";	#to get the number of max layer
 open(FH,$maxLF);
 my $maxL = 0;
 while(my $line = <FH>){
    if($line =~ /^\"(\d+)\+/){
       my $numL = $1;
       if($maxL < $numL){
	  $maxL = $numL;
       }	
    }
 }
 return($maxL);
}





#################
#  Output	#
#################
sub output {
# my @sorted_keys = shift @_;

print <<EOF;
<div id="doc">
   <table width = 1000 border=10 cellpadding=0 cellspacing=0>
   <tr valign = "top">
      <td width = 150>
         <h4 style = "text-align : center;" align = "center">
            <a href = "/index.html" onMouseover = "change(this, '#9966FF', '#330066')" 
	    on Mouseout = "change(this, '#8484ee', '#000033')">
	       Home
	    </a>
         </h4>
         <h4 style = "text-align : center;" align = "center">
            <a href = "/help/help.html" onMouseover = "change(this, '#9966FF', '#330066')" 
	    on Mouseout = "change(this, '#8484ee', '#000033')">
	       Help
	    </a>
         </h4>
         <h4 style = "text-align : center;" align = "center">
EOF
	 #my $bugTarget = "BugDisplay.pl";
	 #if($id != 0){
	    my $bugTarget = "BugReportForm.pl?tool=Glycan Kernel Tool";
	 #}
print <<EOF;
            <a href = "/cgi-bin/tools/Bug/$bugTarget"
            onMouseover = "change(this, '#9966FF', '#330066')" on Mouseout = "change(this, '#8484ee', '#000033')">
	       Feedback
	    </a>
         </h4>
      </td>
      <td width = 800><Font size = 4>
         Your request has been accepted.<br><br><br>

         You will be able to see your result <b>anytime</b> with your calculation id at URL&#58;<br><br>
         <a href="./results.pl">http://www.rings.t.soka.ac.jp/cgi-bin/tools/Kernel/results.pl</a><br><br><br>

         <form action="http://www.rings.t.soka.ac.jp/cgi-bin/tools/Kernel/results.pl" method="get">
            Your Calculation ID&#58; $TIME &nbsp;&nbsp;&nbsp;
            <button type="submit" name="calc_id" value=$TIME> Go </button>
         </form>

         Please make sure to <u>copy this URL address and Calculation ID</u> before closing this page.<br><br><br><br>

      </td>
   </tr>
   </table>
</div>
EOF
}




############################################
#  print Header (Header for error output)  #
############################################
sub printHeader() {
 $STATUS = 2;
 Update_K($DBH, $TIME, $STATUS);

print <<EOF;

   <table width = 1000 border=10 cellpadding=0 cellspacing=0>
   <tr valign = "top">
      <td width = 150>
         <h4 style = "text-align : center;" align = "center">
            <a href = "/index.html" onMouseover = "change(this, '#9966FF', '#330066')" 
	    on Mouseout = "change(this, '#8484ee', '#000033')">
	       Home
	    </a>
         </h4>
         <h4 style = "text-align : center;" align = "center">
            <a href = "/help/help.html" onMouseover = "change(this, '#9966FF', '#330066')" 
	    on Mouseout = "change(this, '#8484ee', '#000033')">
	       Help
	    </a>
         </h4>
         <h4 style = "text-align : center;" align = "center">
EOF
	 #my $bugTarget = "BugDisplay.pl";
	 #if($id != 0){
	    my $bugTarget = "BugReportForm.pl?tool=Glycan Kernel Tool";
	 #}
print <<EOF;
            <a href = "/cgi-bin/tools/Bug/$bugTarget"
            onMouseover = "change(this, '#9966FF', '#330066')" on Mouseout = "change(this, '#8484ee', '#000033')">
	       Feedback
	    </a>
         </h4>
      </td>
      <td align = "center" width = 800><Font size = 4><B>
EOF

 print shift @_; #output error messages.
 print "<B></font></td></tr></table></body></html>";
 exit;
}




#=============================================
# making directory for user management system
#=============================================
sub Make_Dir
{
   my $SAVE_DIR = shift @_;
   if(!-d $SAVE_DIR){
      mkdir ($SAVE_DIR) || die "failed: $!";
   }
}



