#!/usr/bin/perl

use RINGS::Web::HTML;
$id = Get_Cookie_Info();
print "Content-type: text/html\n\n";
print <<EOF;
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

    <meta http-equiv="Content-Style-Type" content="text/css">
    <link rel="stylesheet" href="/rings2.css" type="text/css">

    <title>Glycan Kernel Tool&#58; Result entry</title>

</head>
<Body><!-- alink="#ff0000" bgcolor="#ffffff" link="#0000ef" text="#000000" vlink="#55188a" -->



<style>
   table.mytable { width: 100%; padding: 0px; solid #789DB3;}
   table.mytable th { font-size: 16px; border: 1px;
   vertical-align: middle; padding: 3px; font-weight: bold;}
   table.mytable td { font-size: 14px;
   vertical-align: middle; padding: 7px; }
   table.mytable tr.special td th tr { border-top: 1px solid #ff0000; border-bottom: 1px solid #ff0000; border-left: 1px solid #ff0000; border-right: 1px solid #ff0000; }
</style>

<div id = "menu">
   <p class="glow"><a href="/index.html"><img src="/rings4.png" border=0></a>
   <font size=6> Glycan Kernel Tool&#58; Result entry</font></p>
</div>



<hr>
<table width=1000 border=0 cellpadding=0 cellspacing=0>
   <tr valign="top">
      <td width=150>
         <h4 style="text-align : center;" align="center">
            <a href="/index.html" onMouseover="change(this, '#9966FF', '#330066')" onMouseout="change(this, '#8484ee', '#000033')">Home</a>
         </h4>
         <h4 style="text-align : center;" align="center">
            <a href="/help/help.html" onMouseover="change(this, '#9966FF', '#330066')" onMouseout="change(this, '#8484ee', '#000033')">Help</a>
         </h4>
         <h4 style="text-align : center;" align="center">
EOF
            my $bugTarget = "BugDisplay.pl";
            if($id != 0){
               $bugTarget = "BugReportForm.pl?tool=Glycan Kernel Tool&#58; results entry>";
            }
print <<EOF;
            <a href="/cgi-bin/tools/Bug/$bugTarget" onMouseover="change(this,'#9966FF','#000033')" onMouseout="change(this,'#8484ee','#000033')">Feedback</a>
          </h4>
       </td>

EOF

      my $url_info = $ENV{'QUERY_STRING'};
      my $calc_id = 0;
      if($url_info =~ /calc_id=(.+)/){
          $calc_id = $1;
      }

print <<EOF;

      <form action="/cgi-bin/tools/Kernel/show-results.pl" method="POST" name="KERNEL" enctype="multipart/form-data">
      <td width=1000 border=30 cellpadding=0 cellspacing=0>
         <TABLE border = 3 cellpadding = 3 cellspacing = 0>
            <TBODY>
	       <TR>
                  <TD align = "center"><FONT size = "3">Please input your kernel calculation ID.</FONT>
                     <br><br><div><input type = "text" name = "id" value =$calc_id  size = "50"></div>
		  </TD>
	       </TR>
               <TD colspan = "3" align = "center">
                  <input value="reset" onClick="KERNEL.id.value=''" type="reset">
                  <input value="run" type="submit">
               </TD>
            </TBODY>
         </TABLE>
      </form>
      </td>
   </body>
</html>
EOF
